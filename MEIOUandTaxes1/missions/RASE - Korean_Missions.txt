# Korea Missions

yuan_expedition = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		donggye_area
		north_korea_area
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}
	
	allow = {
		primary_culture = korean
		mil = 4
		any_owned_province = {
			area = central_korea_area
		}
		OR = {
			donggye_area = {
				NOT = { owned_by = ROOT }
			}
			north_korea_area = {
				NOT = { owned_by = ROOT }
			}
		}
	}
	abort = {
		NOT = { 
			any_owned_province = {
				area = central_korea_area
			}
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		add_army_tradition = 15
		every_target_province = {
			add_territorial_core_effect = yes
		}
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}

liaodong_expedition = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		liaoning_area
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}
	
	allow = {
		primary_culture = korean
		mil = 4
		any_owned_province = {
			area = north_korea_area
		}
		liaoning_area = {
			NOT = { owned_by = ROOT }
		}
	}
	abort = {
		NOT = { 
			any_owned_province = {
				area = north_korea_area
			}
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	
	allow = {
		primary_culture = korean
		mil = 4
		owns = 730 # Gangdong
		owns = 2800 # Jangbaek
		NOT = {
			owns = 4035 # Shenyang
		}
	}
	abort = {
		NOT = { owns = 730 } # Gangdong
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_prestige = 5
		every_target_province = {
			add_territorial_core_effect = yes
		}
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}
recover_tamna = {
	
	type = country
	
	category = DIP
	ai_mission = yes
	
	allow = {
		OR = {
			tag = KOR
			tag = JOS
		}
		exists = TMN
		is_free_or_tributary_trigger = yes
		TMN = {
			is_free_or_tributary_trigger = yes
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = TMN }
			TMN = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		OR = {
			TMN = { vassal_of = KOR }
			TMN = { vassal_of = JOS }
		}
	}
	chance = {
		factor = 1000
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = TMN
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = TMN
		}
	}
	effect = {
		add_prestige = 10
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = TMN
			}
		}
	}
}
take_goryeo = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = CSE
		exists = KOR
		KOR = { is_free_or_tributary_trigger = yes }
		is_free_or_tributary_trigger = yes
		dynasty = "Wang"
	}
	abort = {
		NOT = { exists = KOR }
		NOT = { dynasty = "Wang" }
	}
	success = {
		KOR = { is_subject_of = CSE }
	}
	chance = {
		factor = 1000
	}
	immediate = {
		add_casus_belli = {
			type = cb_restore_personal_union
			months = 240
			target = KOR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_restore_personal_union
			target = KOR
		}
	}
	effect = {
		add_prestige = 10
		hidden_effect = {
			remove_casus_belli = {
				type = cb_restore_personal_union
				target = KOR
			}
		}
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		inherit = KOR
		change_tag = KOR
		set_capital = 1822
		swap_free_idea_group = yes
	}
}
