# Government Missions

# sliders?
# government types?
# technology research.

go_legitimate = {
	
	type = country
	
	category = ADM
	
	allow = {
		government = monarchy
		has_regency = no
		NOT = { legitimacy = 50 }
		is_at_war = no
		NOT = { is_nomad = yes }
	}
	abort = {
		OR = {
			NOT = { government = monarchy }
			has_regency = yes
		}
	}
	immediate = {
		add_country_modifier = {
			name = "improved_legitimacy_mission"
			duration = -1
		}
	}
	abort_effect = {
		remove_country_modifier = improved_legitimacy_mission
		add_country_modifier = {
			name = "improved_legitimacy_mission_malus"
			duration = 1825
		}
	}
	success = {
		legitimacy = 75
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.05
			NOT = { legitimacy = 50 }
		}
		modifier = {
			factor = 1.05
			NOT = { legitimacy = 60 }
		}
		modifier = {
			factor = 1.05
			NOT = { legitimacy = 70 }
		}
		modifier = {
			factor = 1.05
			NOT = { legitimacy = 80 }
		}
		
	}
	effect = {
		add_prestige = 5
		remove_country_modifier = improved_legitimacy_mission
		add_ruler_modifier = {
			name = "legitimacy_defended"
		}
		add_adm_power = 20
		add_dip_power = 20
	}
}

make_core_province = {
	
	type = our_provinces
	
	category = ADM
	
	allow = {
		always = no
		is_territorial_core = owner
		NOT = { is_state_core = owner }
		owner = { is_at_war = no }
		is_colony = no
		NOT = { has_construction = core }
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	success = {
		is_core = owner
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			base_tax = 5
		}
	}
	effect = {
		owner = {
			add_adm_power = 35
		}
	}
}

#change_province_culture = {
#
#	type = our_provinces
#	
#	category = ADM
#	
#	allow = {
#		always = no
#		has_owner_culture = no
#		has_owner_accepted_culture = no
#		is_core = owner
#		has_owner_religion = yes
#		owner = { is_at_war = no }
#		is_colony = no
#		NOT = { has_construction = culture }
#		owner = {
#			NOT = { accepted_culture = ROOT }
#		}
#	}
#	abort = {
#		NOT = { owned_by = FROM }
#	}
#	success = {
#		culture = owner
#	}
#	chance = {
#		factor = 1000
#		modifier = {
#			factor = 1.5
#			base_tax = 5
#		}
#		modifier = {
#			factor = 0.1
#			owner = {
#				culture_group = ROOT
#			}
#		}
#	}
#	effect = {
#		owner = {
#			add_dip_power = 10
#		}
#	}
#}
