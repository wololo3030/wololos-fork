# Bavarian Missions

bavaria_austria_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = BAV
		exists = HAB
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { war_with = HAB }
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { marriage_with = HAB }
		HAB = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			war_with = HAB
			HAB = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = HAB
		HAB = { has_opinion = { who = BAV value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HAB value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HAB value = -100 } }
		}
	}
	effect = {
		add_stability_1 = yes
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}


conquer_franken = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		1366
	}
	allow = {
		tag = BAV
		is_free_or_tributary_trigger = yes
		mil = 4
		1366 = { NOT = { owned_by = ROOT } } # Wurzburg
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1366 # Wurzburg
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 1366 # Wurzburg
	}
	abort_effect = {
		remove_claim = 1366 # Wurzburg
	}
	effect = {
		add_prestige = 5
		1366 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}


conquer_pfalz = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		1361
	}
	allow = {
		tag = BAV
		is_free_or_tributary_trigger = yes
		mil = 4
		1361 = {		# Pfalz
			NOT = { owned_by = ROOT }
			any_neighbor_province = { owned_by = ROOT }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1361 # Oberpfalz
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 1361
	}
	abort_effect = {
		remove_claim = 1361
	}
	effect = {
		add_prestige = 5
		1361 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}


conquer_alzey = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		78
	}
	allow = {
		tag = BAV
		is_free_or_tributary_trigger = yes
		mil = 4
		78 = {	# Rheinpfalz
			NOT = { owned_by = ROOT }
			any_neighbor_province = { owned_by = ROOT }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 78 # Rheinpfalz
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 78
	}
	abort_effect = {
		remove_claim = 78
	}
	effect = {
		add_prestige = 5
		78 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}


conquer_ansbach = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		1360
	}
	allow = {
		tag = BAV
		is_free_or_tributary_trigger = yes
		mil = 4
		1360 = {	# Ansbach
			NOT = { owned_by = ROOT }
			any_neighbor_province = { owned_by = ROOT }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1360 # Ansbach
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 1360 # Ansbach
	}
	abort_effect = {
		remove_claim = 1360 # Ansbach
	}
	effect = {
		add_stability_1 = yes
		1360 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
			add_territorial_core_effect = yes
		}
	}
}
