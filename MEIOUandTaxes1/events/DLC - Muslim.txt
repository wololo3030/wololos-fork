########################################
# Muslim DLC Events
#
# written by Sara Wendel-�rtqvist
########################################

namespace = muslim_dlc

# Ramadan #REMOVED BY DEI GRATIA
# Eid al-Fitr #REMOVED BY DEI GRATIA
# Invited to Eid al-Fitr #REMOVED BY DEI GRATIA
# Hajj - Start #REMOVED BY DEI GRATIA
# Tawaf - Arafat - Muzdalifah - Ramy al-Jamarat - Eid al-Adha - Tawaf Al-Ifaadah
# Hajj - End - Tawaf al-Wida #REMOVED BY DEI GRATIA
# Reparing the Masjid al-Haram #REMOVED BY DEI GRATIA
country_event = {
	id = muslim_dlc.7
	title = "muslim_dlc.EVTNAME7"
	desc = "muslim_dlc.EVTDESC7"
	picture = GREAT_BUILDING_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		owns = 385 # Mecca
		religion_group = muslim
		is_year = 1500
		NOT = { is_year = 1600 }
		treasury = 200
		NOT = { has_ruler_modifier = repaired_masjid_al_haram }
		NOT = { is_advisor_employed = 1191 }
		NOT = { has_country_flag = mimar_sinan_flag }
	}
	
	mean_time_to_happen = { years = 166 }
	
	immediate = {
		set_country_flag = mimar_sinan_flag
	}
	
	option = {
		name = "muslim_dlc.EVTOPTA7"
		define_advisor = {
			name = "Mimar Sinan"
			religion = shiite
			type = artist
			skill = 3
			discount = yes
		}
		add_years_of_income = -0.5
		add_ruler_modifier = { name = repaired_masjid_al_haram }
	}
	option = {
		name = "muslim_dlc.EVTOPTB7"
		add_years_of_income = -0.2
		add_ruler_modifier = { name = repaired_masjid_al_haram }
	}
	option = {
		name = "muslim_dlc.EVTOPTC7"
		#		add_piety = -0.1
	}
}

# Suhrawardiyya #REMOVED BY DEI GRATIA

# Policy changes - Ban alcohol, coffee and tobacco
country_event = {
	id = muslim_dlc.9
	title = "muslim_dlc.EVTNAME9"
	desc = "muslim_dlc.EVTDESC9"
	picture = REFORM_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion_group = muslim
		is_year = 1650
		NOT = { is_year = 1750 }
		OR = {
			NOT = { dip = 3 }
			NOT = { adm = 3 }
		}
		OR = { has_regency = no has_consort_regency = yes }
		NOT = { has_ruler_modifier = banned_alcohol_tobacco_coffee }
		NOT = { has_country_flag = policy_changes_flag }
	}
	
	mean_time_to_happen = { years = 166 }
	
	immediate = {
		hidden_effect = {
			set_country_flag = policy_changes_flag
		}
	}
	
	option = {
		name = "muslim_dlc.EVTOPTA9"
		add_ruler_modifier = { name = banned_alcohol_tobacco_coffee }
		add_adm_power = 25
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTB9"
		add_ruler_modifier = { name = tax_alcohol_tobacco_coffee }
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTC9"
		add_adm_power = 25
	}
}

# Enforced morales � la Aurangzeb
country_event = {
	id = muslim_dlc.10
	title = "muslim_dlc.EVTNAME10"
	desc = "muslim_dlc.EVTDESC10"
	picture = REFORM_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion_group = muslim
		is_year = 1700
		NOT = {
			is_year = 1800
		}
		NOT = {
			piety = 75
		}
		NOT = { has_country_flag = policy_changes_flag }
		NOT = { has_ruler_modifier = banned_alcohol_tobacco_coffee }
	}
	
	mean_time_to_happen = { years = 166 }
	
	immediate = {
		hidden_effect = {
			set_country_flag = policy_changes_flag
		}
	}
	
	option = {
		name = "muslim_dlc.EVTOPTA10"
		add_ruler_modifier = { name = banned_alcohol_tobacco_coffee }
		add_adm_power = 25
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTB10"
		add_ruler_modifier = { name = tax_alcohol_tobacco_coffee }
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTC10"
		add_adm_power = 25
	}
}

# Muhammad Salih Tahtawi: The Celestial Globe
country_event = {
	id = muslim_dlc.11
	title = "muslim_dlc.EVTNAME11"
	desc = "muslim_dlc.EVTDESC11"
	picture = ADVISOR_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion_group = muslim
		is_year = 1550
		NOT = {
			is_year = 1600
		}
		NOT = { has_country_modifier = celestial_globe }
		NOT = { has_country_modifier = tahtawi_timer }
	}
	
	mean_time_to_happen = { years = 166 }
	
	option = {
		name = "muslim_dlc.EVTOPTA11"
		add_country_modifier = {
			name = celestial_globe
			duration = 7300
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTB11"
		define_advisor = {
			name = "Muhammad Salih Tahtawi"
			religion = shiite
			type = artist
			skill = 3
			discount = yes
		}
		hidden_effect = {
			add_country_modifier = {
				name = tahtawi_timer
				duration = 3650
				hidden = yes
			}
		}
	}
}

# Mian Mir #REMOVED BY DEI GRATIA
# Ayazmakapi Fire #REMOVED BY DEI GRATIA
# Chishti Order #REMOVED BY DEI GRATIA
# Qadiriyya #REMOVED BY DEI GRATIA
# Islamic Law (Sharia) #REMoVED BY DEI GRATIA
# Ghulams
country_event = {
	id = muslim_dlc.17
	title = "muslim_dlc.EVTNAME17"
	desc = "muslim_dlc.EVTDESC17"
	picture = REVOLUTION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion_group = muslim
		NOT = { has_country_modifier = ghulam_timer }
		OR = {
			is_at_war = yes
			any_known_country = {
				truce_with = ROOT
			}
		}
	}
	
	mean_time_to_happen = { years = 166 }
	
	immediate = {
		hidden_effect = {
			add_country_modifier = {
				name = ghulam_timer
				duration = 7300
				hidden = yes
			}
		}
	}
	
	option = {
		name = "muslim_dlc.EVTOPTA17"
		add_adm_power = 20
	}
	option = {
		name = "muslim_dlc.EVTOPTB17"
		add_army_tradition = 5
		add_navy_tradition = 5
		add_mil_power = 20
	}
	option = {
		name = "muslim_dlc.EVTOPTB17"
		add_dip_power = 40
	}
}

# Reza Abbasi #REMOVED BY DEI GRATIA
# Baha al-din al-Amili #REMOVED BY DEI GRATIA

# Convert Hagia Sophia into a Mosque
country_event = {
	id = muslim_dlc.20
	title = "muslim_dlc.EVTNAME20"
	desc = "muslim_dlc.EVTDESC20"
	picture = MOSQUE_HAGA_SOPHIA_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion_group = muslim
		is_year = 1453
		owns = 1402 # Thrace/Constantinople/Istanbul
	}
	
	mean_time_to_happen = { years = 166 }
	
	option = {
		name = "muslim_dlc.EVTOPTA20"
		#		add_piety = 0.1
		add_prestige = 10
		add_years_of_income = -0.1
		1402 = {
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = christian
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = christian
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = christian
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = christian
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = christian
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			add_province_modifier = {
				name = "fanatics_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
			if = {
				limit = {
					owner = {
						religion = sunni
					}
				}
				change_religion = sunni
			}
			if = {
				limit = {
					owner = {
						religion = shiite
					}
				}
				change_religion = shiite
			}
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTB20"
		ai_chance = {
			factor = 0
		}
		#		add_piety = -0.1
		1402 = {
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = muslim
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = muslim
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
			random_neighbor_province = {
				limit = {
					owned_by = ROOT
					religion_group = muslim
					NOT = { has_province_modifier = fanatics_organizing }
				}
				add_province_modifier = {
					name = "fanatics_organizing"
					duration = 1825
				}
				hidden_effect = {
					set_variable = { which = added_unrest value = 5 }
					add_base_unrest = yes
				}
				custom_tooltip = added_unrest_5
			}
		}
	}
}

# Madrassa #REMOVED BY DEI GRATIA
# Zubaidah bint Ja'far #REMOVED BY DEI GRATIA
# Serafeddin Sabuncuoglu #REMOVED BY DEI GRATIA
# Al-Birjandi #REMOVED BY DEI GRATIA
# People of the Book - Dhimmi #REMOVED BY DEI GRATIA
# Mahdi #REMOVED BY DEI GRATIA
# Shiite vs Sunni (Shiite main religion)
country_event = {
	id = muslim_dlc.27
	title = "muslim_dlc.EVTNAME27"
	desc = "muslim_dlc.EVTDESC27"
	picture = RELIGION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion = shiite
		num_of_owned_provinces_with = {
			value = 10
			religion = shiite
		}
		num_of_owned_provinces_with = {
			value = 10
			religion = sunni
		}
	}
	
	mean_time_to_happen = { years = 166 }
	
	option = {
		name = "muslim_dlc.EVTOPTA27"
		every_owned_province = {
			limit = { religion = sunni }
			add_province_modifier = {
				name = "fanatics_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTB27"
		every_owned_province = {
			limit = {
				religion = shiite
			}
			add_province_modifier = {
				name = "fanatics_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
}

# Sunni vs shiite (Sunni main religion)
country_event = {
	id = muslim_dlc.28
	title = "muslim_dlc.EVTNAME28"
	desc = "muslim_dlc.EVTDESC28"
	picture = RELIGION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_dlc = "Star and Crescent"
		religion = shiite
		num_of_owned_provinces_with = {
			value = 10
			religion = shiite
		}
		num_of_owned_provinces_with = {
			value = 10
			religion = sunni
		}
	}
	
	mean_time_to_happen = { years = 166 }
	
	option = {
		name = "muslim_dlc.EVTOPTA27"
		every_owned_province = {
			limit = { religion = sunni }
			add_province_modifier = {
				name = "fanatics_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "muslim_dlc.EVTOPTB27"
		every_owned_province = {
			limit = {
				religion = sunni
			}
			add_province_modifier = {
				name = "fanatics_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
}
