########################################
# Flavor events for China (MNG)
#
# written by Sara Wendel-�rtqvist
########################################


# The Strategic Decision after Zheng He's Journey
country_event = {
	id = flavor_mng.1
	title = "flavor_mng.EVTNAME1"
	desc = "flavor_mng.EVTDESC1"
	picture = MERCHANTS_TALKING_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_modifier = treasury_fleet
		is_year = 1435
		NOT = { is_year = 1470 }
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA1" #Inward Perfection
		add_faction_influence = { faction = temples influence = 5 }
	}
	option = {
		name = "flavor_mng.EVTOPTB1" #Balanced Position
		add_faction_influence = { faction = enuchs influence = 5 }
	}
	option = {
		name = "flavor_mng.EVTOPTC1" #Outward Expansion
		add_faction_influence = { faction = bureaucrats influence = 5 }
	}
}

# Repairing the Great Wall
#country_event = {
#	id = flavor_mng.2
#	title = "flavor_mng.EVTNAME2"
#	desc = "flavor_mng.EVTDESC2"
#	picture = REVOLUTION_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		has_country_flag = mandate_of_heaven_claimed
#		is_year = 1450
#		NOT = { is_year = 1490 }
#	}
#	
#	mean_time_to_happen = {
#		months = 200
#	}
#	
#	option = {
#		name = "flavor_mng.EVTOPTA2" #Yes, we need to repair the Great Wall
#		random_list = {
#			30 = {}
#			40 = {
#				subtract_stability_points_50 = yes
#			}
#			30 = {
#				subtract_stability_1 = yes
#			}
#		}
#		add_years_of_income = -0.25
#		add_army_tradition = 5
#		set_country_flag = mng_great_wall_flag
#	}
#	option = {
#		name = "flavor_mng.EVTOPTB2" #Let it continue to crumble
#		add_stability_1 = yes
#		add_yearly_manpower = 0.5
#	}
#}

#Further Repairs for the Great Wall
#country_event = {
#	id = flavor_mng.3
#	title = "flavor_mng.EVTNAME3"
#	desc = "flavor_mng.EVTDESC3"
#	picture = REVOLUTION_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		has_country_flag = mandate_of_heaven_claimed
#		is_year = 1500
#		NOT = { is_year = 1540 }
#		has_country_flag = mng_great_wall_flag
#	}
#	
#	mean_time_to_happen = {
#		months = 200
#	}
#	
#	option = {
#		name = "flavor_mng.EVTOPTA2" #Yes, we need to repair the Great Wall
#		random_list = {
#			30 = {}
#			40 = {
#				subtract_stability_points_50 = yes
#			}
#			30 = {
#				subtract_stability_1 = yes
#			}
#		}
#		add_years_of_income = -0.25
#		add_army_tradition = 5
#		clr_country_flag = mng_great_wall_flag
#	}
#	option = {
#		name = "flavor_mng.EVTOPTB2" #Let it continue to crumble
#		add_stability_1 = yes
#		add_yearly_manpower = 0.5
#		clr_country_flag = mng_great_wall_flag
#	}
#}

# The Perfection of Ming Porcelain - removed, duplicate with Urban Goods events.
#country_event = {
#	id = flavor_mng.4
#	title = "flavor_mng.EVTNAME4"
#	desc = "flavor_mng.EVTDESC4"
#	picture = IMPORTANT_STATUE_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		has_country_flag = mandate_of_heaven_claimed
#		is_year = 1500
#		NOT = { is_year = 1550 }
#	}
#	
#	mean_time_to_happen = {
#		months = 200
#	}
#	
#	option = {
#		name = "flavor_mng.EVTOPTA4"
#		random_owned_province = {
#			limit = {
#				asia_continent_trigger = yes
#				NOT = {
#					trade_goods = chinaware
#				}
#			}
#			change_trade_goods = chinaware
#		}
#	}
#}

# The Closure of China
country_event = {
	id = flavor_mng.5
	title = "flavor_mng.EVTNAME5"
	desc = "flavor_mng.EVTDESC5"
	picture = MERCHANTS_TALKING_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1555
		NOT = { is_year = 1600 }
		any_country = {
			capital_scope = {
				continent = europe
			}
			any_known_country = {
				has_country_flag = mandate_of_heaven_claimed
			}
			any_active_trade_node = {
				owned_by = ROOT
			}
		}
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA5" #Hold a small breathing hole open
		add_prestige = -1
	}
	option = {
		name = "flavor_mng.EVTOPTB5" #Close China to the outside world
		add_mercantilism = 40
		add_legitimacy = 10
		add_stability_1 = yes
		add_country_modifier = {
			name = mng_closed_china
			duration = 36500
		}
	}
	option = {
		name = "flavor_mng.EVTOPTC5" #Keep trade open for all
		add_country_modifier = {
			name = mng_open_china
			duration = 36500
		}
	}
}

# Qi Jiguang's Army Reforms
#country_event = {
#	id = flavor_mng.6
#	title = "flavor_mng.EVTNAME6"
#	desc = "flavor_mng.EVTDESC6"
#	picture = REFORM_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		has_country_flag = mandate_of_heaven_claimed
#		is_year = 1558
#		NOT = { is_year = 1588 }
#	}
#	
#	mean_time_to_happen = {
#		months = 200
#	}
#	
#	option = {
#		name = "flavor_mng.EVTOPTA6" #Go with the reforms
#		add_mil_power = 100
#	}
#	option = {
#		name = "flavor_mng.EVTOPTB6" #No, such reforms would be too costly
#		add_stability_1 = yes
#	}
#}

#The Arrival of the Jesuits
country_event = {
	id = flavor_mng.7
	title = "flavor_mng.EVTNAME7"
	desc = "flavor_mng.EVTDESC7"
	picture = RELIGION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1575
		NOT = { is_year = 1600 }
		any_known_country = {
			NOT = {
				has_country_flag = mandate_of_heaven_claimed
			}
			has_country_modifier = religious_tolerance
		}
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA7" #Yes, we may benefit from their learning
		add_dip_power = 25
		add_mil_power = 25
		add_adm_power = 25
		random_list = {
			30 = {}
			40 = {
				subtract_stability_points_50 = yes
			}
			30 = {
				subtract_stability_1 = yes
			}
		}
		add_country_modifier = {
			name = "religious_tolerance"
			duration = -1
		}
		every_known_country = {
			limit = {
				NOT = {
					has_country_flag = mandate_of_heaven_claimed
				}
				has_country_modifier = religious_tolerance
			}
			
			add_opinion = {
				who = ROOT
				modifier = opinion_accepted_jesuits
			}
		}
	}
	option = {
		name = "flavor_mng.EVTOPTB7"
		add_stability_1 = yes
	}
}

#The Manchu Rebellion
#country_event = {
#	id = flavor_mng.9
#	title = "flavor_mng.EVTNAME9"
#	desc = "flavor_mng.EVTDESC9"
#	picture = ANGRY_MOB_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		has_country_flag = mandate_of_heaven_claimed
#		is_year = 1600
#		NOT = { is_year = 1630 }
#		NOT = { stability = 3 }
#		OR = {
#			NOT = {
#				stability = 0
#			}
#			AND = {
#				NOT = {
#					DIP = 3
#				}
#				NOT = {
#					ADM = 3
#				}
#				NOT = {
#					MIL = 3
#				}
#				has_regency = no
#			}
#			war_exhaustion = 2
#		}
#	}
#	
#	mean_time_to_happen = {
#		months = 200
#		modifier = {
#			factor = 0.5
#			has_country_flag = famine_in_ming
#		}
#		modifier = {
#			factor = 2
#			has_country_flag = fight_against_famine_in_ming
#		}
#		modifier = {
#			factor = 0.5
#			MCH = { exists = no }
#		}
#	}
#	
#	option = {
#		name = "flavor_mng.EVTOPTA9" #We are too weak to crush the rebels
#		ai_chance = { factor = 80 }
#		every_owned_province = {
#			limit = {
#				culture = manchu
#			}
#			add_core = MCH
#		}
#		if = {
#			limit = {
#				MCH = { exists = yes }
#			}
#			every_owned_province = {
#				limit = {
#					culture = manchu
#				}
#				nationalist_rebels = 3
#				change_controller = REB
#			}
#		}
#		if = {
#			limit = {
#				MCH = { exists = no }
#			}
#			every_owned_province = {
#				limit = {
#					culture = manchu
#				}
#				nationalist_rebels = 3
#				change_controller = REB
#			}
#		}
#	}
#	option = {
#		name = "flavor_mng.EVTOPTB9" #Attempt to crush the rebels
#		ai_chance = { factor = 20 }
#		add_manpower = -20
#		add_years_of_income = -0.5
#		every_owned_province = {
#			limit = {
#				culture = manchu
#			}
#			add_core = MCH
#		}
#		if = {
#			limit = {
#				MCH = { exists = yes }
#			}
#			every_owned_province = {
#				limit = {
#					culture = manchu
#				}
#				nationalist_rebels = 10
#			}
#		}
#		if = {
#			limit = {
#				MCH = { exists = no }
#			}
#			every_owned_province = {
#				limit = {
#					culture = manchu
#				}
#				nationalist_rebels = 10
#			}
#		}
#	}
#}

#The Cult of the White Lotus
country_event = {
	id = flavor_mng.10
	title = "flavor_mng.EVTNAME10"
	desc = "flavor_mng.EVTDESC10"
	picture = ANGRY_MOB_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1600
		NOT = { is_year = 1625 }
		NOT = { stability = 3 }
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA10" # Bah, surely these cultists can do no lasting harm!
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	option = {
		name = "flavor_mng.EVTOPTB10" #Let the forces of the Empire crush the Rebellion!
		add_years_of_income = -0.5
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				asia_continent_trigger = yes
				NOT = { has_province_modifier = peasants_organizing }
			}
			add_province_modifier = {
				name = "peasants_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
}

#The Financial Crisis
country_event = {
	id = flavor_mng.11
	title = "flavor_mng.EVTNAME11"
	desc = "flavor_mng.EVTDESC11"
	picture = ECONOMY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1640
		NOT = { is_year = 1660 }
		num_of_loans = 5 #More loans?
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA11" #Yes, let us raise new taxes
		add_inflation = 5
		add_years_of_income = 3
		random_list = {
			30 = {}
			40 = {
				subtract_stability_points_50 = yes
			}
			30 = {
				subtract_stability_1 = yes
			}
		}
	}
	option = {
		name = "flavor_mng.EVTOPTB11" #No, our subjects are already angry enough!
		add_stability_1 = yes
	}
}

#Li Zicheng's Rebellion # Mod
#country_event = {
#	id = flavor_mng.12
#	title = "flavor_mng.EVTNAME12"
#	desc = "flavor_mng.EVTDESC12"
#	picture = ANGRY_MOB_eventPicture
#	
#	fire_only_once = yes
#	
#	trigger = {
#		tag = MNG
#		is_year = 1600
#		NOT = { is_year = 1660 }
#		NOT = { stability = 3 }
#		has_country_flag = shun_rebellion
#		OR = {
#			owns = 2252 # Xian
#			owns = 701  # Kaifeng
#			owns = 702  # Luoyang
#			owns = 2471 # Hanzhong
#			owns = 2605 # Nanyang
#		}
#	}
#	
#	mean_time_to_happen = {
#		months = 200
#	}
#	
#	immediate = {
#		hidden_effect = {
#			2252 = {
#				add_core = SHU
#			}
#			701 = {
#				add_core = SHU
#			}
#			702 = {
#				add_core = SHU
#			}
#			2471 = {
#				add_core = SHU
#			}
#			2605 = {
#				add_core = SHU
#			}
#			random_owned_province = {
#				limit = {
#					asia_continent_trigger = yes
#					is_core = MNG
#				}
#				set_province_flag = MNG_zechang_rebellion_flag1
#			}
#			random_owned_province = {
#				limit = {
#					asia_continent_trigger = yes
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag1
#					}
#				}
#				set_province_flag = MNG_zechang_rebellion_flag2
#			}
#			random_owned_province = {
#				limit = {
#					asia_continent_trigger = yes
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag1
#					}
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag2
#					}
#				}
#				set_province_flag = MNG_zechang_rebellion_flag3
#			}
#			random_owned_province = {
#				limit = {
#					asia_continent_trigger = yes
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag1
#					}
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag2
#					}
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag3
#					}
#				}
#				set_province_flag = MNG_zechang_rebellion_flag4
#			}
#			random_owned_province = {
#				limit = {
#					asia_continent_trigger = yes
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag1
#					}
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag2
#					}
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag3
#					}
#					NOT = {
#						has_province_flag = MNG_zechang_rebellion_flag4
#					}
#				}
#				set_province_flag = MNG_zechang_rebellion_flag5
#			}
#		}
#	}
#	
#	option = {
#		name = "flavor_mng.EVTOPTA12" #Let us attempt to destroy the traitor!
#		subtract_stability_3 = yes
#		release = SHU
#		SHU = {
#			define_ruler = {
#				name = "Zicheng"
#				dynasty = "Li"
#				adm = 2
#				dip = 3
#				mil = 4
#			}
#		}
#		if = {
#			limit = {
#				SHU = { owns = 2252 }
#			}
#			Effect_set_capital = { target=2252 } # Xian
#			hidden_effect = {
#				country_event = {
#					id = miscexpenses.001
#				}
#			}
#		}
#		shanxi_area = {
#			add_core = SHU
#		}
#		shaanxi_area = {
#			add_core = SHU
#		}
#		gansu_area = {
#			add_core = SHU
#		}
#		hebei_area = {
#			add_claim = SHU
#		}
#		jiangsu_area = {
#			add_claim = SHU
#		}
#		anhui_area = {
#			add_claim = SHU
#		}
#		shandong_area = {
#			add_claim = SHU
#		}
#		SHU = {
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			cavalry = 2252
#			cavalry = 2252
#			cavalry = 2252
##		}
#		SHU = {
#			declare_war_with_cb = {
#				who = MNG
#				casus_belli = cb_independence_war
#			}
#		}
#	}
#	option = {
#		name = "flavor_mng.EVTOPTB12"
#		random_list = {
#			30 = {}
#			40 = {
#				subtract_stability_points_50 = yes
#			}
#			30 = {
#				subtract_stability_1 = yes
#			}
#		}
#		add_years_of_income = -0.5
#		add_legitimacy = -30
#		add_prestige = -30
#		release = SHU
#		SHU = {
#			define_ruler = {
#				name = "Zicheng"
#				dynasty = "Li"
#				adm = 2
#				dip = 3
#				mil = 4
#			}
#		}
#		if = {
#			limit = {
#				SHU = { owns = 2252 }
#			}
#			Effect_set_capital = { target=2252 } # Xian
#			hidden_effect = {
#				country_event = {
#					id = miscexpenses.001
#				}
#			}
#		}
#		every_owned_province = {
#			limit = {
#				OR = {
#					area = shanxi_area
#					area = gansu_area
#					area = henan_area
#				}
#			}
#			cede_province = SHU
#		}
#		SHU = {
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			infantry = 2252
#			cavalry = 2252
#			cavalry = 2252
#			cavalry = 2252
#		}
#	}
#}
#The Expulsion of the Jesuits
country_event = {
	id = flavor_mng.13
	title = "flavor_mng.EVTNAME13"
	desc = "flavor_mng.EVTDESC13"
	picture = RELIGION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1720
		NOT = { is_year = 1745 }
		has_country_modifier = religious_tolerance
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA13"
		remove_country_modifier = religious_tolerance
		add_stability_1 = yes
	}
	option = {
		name = "flavor_mng.EVTOPTB13"
		add_dip_power = 50
	}
}

#The Appointment of Heshen
country_event = {
	id = flavor_mng.14
	title = "flavor_mng.EVTNAME14"
	desc = "flavor_mng.EVTDESC14"
	picture = ADVISOR_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		owns = 708 # Beijin, was 649 - Ternate
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1770
		NOT = { is_year = 1800 }
		NOT = { advisor_exists = 380 }
		NOT = { is_advisor_employed = 380 }
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA14" #Appoint him despite his faults
		define_advisor = {
			type = statesman
			location = 708 # Heshen was born and lived in Beijing
			name = "Heshen"
			skill = 2
			discount = yes
		}
	}
	option = {
		name = "flavor_mng.EVTOPTB14" #No, such corruption could ruin the state!
		add_prestige = -1
	}
}

#Lord Macartney's Mission
country_event = {
	id = flavor_mng.15
	title = "flavor_mng.EVTNAME15"
	desc = "flavor_mng.EVTDESC15"
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		owns = 708 # Beijin, was 649 - Ternate
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1775
		NOT = { is_year = 1800 }
		any_owned_province = {
			OR = {
				is_strongest_trade_power = ENG
				is_strongest_trade_power = GBR
			}
		}
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA15" #Reject the overtures of these rude western barbarians!
		add_stability_1 = yes
	}
	option = {
		name = "flavor_mng.EVTOPTB15" #We are interested in their ideas
		random_list = {
			30 = {}
			40 = {
				subtract_stability_points_50 = yes
			}
			30 = {
				subtract_stability_1 = yes
			}
		}
		GBR = {
			add_opinion = {
				who = ROOT
				modifier = opinion_ideas
			}
		}
		add_adm_power = 50
		add_dip_power = 50
	}
}

#The White Lotus Rebellion
country_event = {
	id = flavor_mng.16
	title = "flavor_mng.EVTNAME16"
	desc = "flavor_mng.EVTDESC16"
	picture = ANGRY_MOB_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		owns = 708 # Beijin, was 649 - Ternate
		has_country_flag = mandate_of_heaven_claimed
		is_year = 1780
		NOT = { is_year = 1800 }
	}
	
	mean_time_to_happen = { years = 16 }
	
	option = {
		name = "flavor_mng.EVTOPTA16" #It's likely just harmless talk
		random_list = {
			30 = {}
			40 = {
				subtract_stability_points_50 = yes
			}
			30 = {
				subtract_stability_1 = yes
			}
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
	}
	option = {
		name = "flavor_mng.EVTOPTB16" #Crush these plotters!
		add_years_of_income = -0.50
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
		random_core_province = {
			limit = {
				NOT = { has_province_modifier = turmoil_organizing }
				asia_continent_trigger = yes
			}
			add_province_modifier = {
				name = "turmoil_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
	}
}

# No more Celestial Empire
#country_event = {
#	id = flavor_mng.17
#	title = flavor_mng.17.t
#	desc = flavor_mng.17.d
#	picture = COURT_eventPicture
#	
#	is_triggered_only = yes
#	
#	trigger = {
#		technology_group = western
#		has_factions = yes
#	}
#	
#	option = {
#		name = flavor_mng.17.a
#		remove_faction = bureaucrats
#		remove_faction = temples
#		remove_faction = enuchs
#		if = {
#			limit = {
#				has_country_modifier = low_tax_rate
#			}
#			remove_country_modifier = low_tax_rate
#		}
#		if = {
#			limit = {
#				has_country_modifier = medium_tax_rate
#			}
#			remove_country_modifier = medium_tax_rate
#		}
#		if = {
#			limit = {
#				has_country_modifier = high_tax_rate
#			}
#			remove_country_modifier = high_tax_rate
#		}
#		if = {
#			limit = {
#				has_country_modifier = chinese_physiocratic_economy
#			}
#			remove_country_modifier = chinese_physiocratic_economy
#		}
#		if = {
#			limit = {
#				has_country_modifier = chinese_mercantilistic_economy
#			}
#			remove_country_modifier = chinese_mercantilistic_economy
#		}
#		if = {
#			limit = {
#				has_country_modifier = wei_suo_system
#			}
#			remove_country_modifier = wei_suo_system
#		}
#		if = {
#			limit = {
#				has_country_modifier = general_trained_militia
#			}
#			remove_country_modifier = general_trained_militia
#		}
#		if = {
#			limit = {
#				has_country_modifier = treasury_fleet
#			}
#			remove_country_modifier = treasury_fleet
#		}
#		if = {
#			limit = {
#				has_country_modifier = reformed_nurgan_comission
#			}
#			remove_country_modifier = reformed_nurgan_comission
#		}
#		if = {
#			limit = {
#				chinese_imperial_gov_trigger = yes
#			}
#			change_government = feudal_monarchy
#			set_government_rank = 6
#		}
#		add_absolutism = 10
#	}
#}

