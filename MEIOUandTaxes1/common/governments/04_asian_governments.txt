##############################     Asian     ##############################
##############################  Governments  ##############################



japanese_monarchy = {
	monarchy = yes
	
	color = { 240 120 5 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_vassal_war = no
	allow_vassal_alliance = no
	allow_convert = no
	nation_designer_trigger = {
		culture_group = japanese
	}
	maintain_dynasty = yes
	
	max_states = 3
	
	rank = {
		1 = {
			stability_cost_modifier = 0.10
			
			land_maintenance_modifier = -0.2
			fort_maintenance_modifier = -0.2
			army_tradition_from_battle = 0.5
			
			max_absolutism = -20
		} #Emperor
		2 = {
			stability_cost_modifier = 0.10
			
			land_maintenance_modifier = -0.2
			fort_maintenance_modifier = -0.2
			army_tradition_from_battle = 0.5
			
			max_absolutism = -20
		} #Shogun
		3 = {
			stability_cost_modifier = 0.10
			
			land_maintenance_modifier = -0.2
			fort_maintenance_modifier = -0.2
			army_tradition_from_battle = 0.5
			
			max_absolutism = -20
		} #Shogun contender
		4 = {
			stability_cost_modifier = 0.10
			
			land_maintenance_modifier = -0.2
			fort_maintenance_modifier = -0.2
			army_tradition_from_battle = 0.5
			
			max_absolutism = -20
		} #Daimyo
		5 = {
			stability_cost_modifier = 0.10
			
			land_maintenance_modifier = -0.2
			fort_maintenance_modifier = -0.2
			army_tradition_from_battle = 0.5
			
			max_absolutism = -20
		} #Daimyo
		6 = {
			stability_cost_modifier = 0.10
			
			land_maintenance_modifier = -0.2
			fort_maintenance_modifier = -0.2
			army_tradition_from_battle = 0.5
			
			max_absolutism = -20
		} #Daimyo
	}
}

eastern_monarchy = {
	monarchy = yes
	
	color = { 200 150 80 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	
	max_states = 2
	
	rank = {
		1 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		2 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		3 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		4 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		5 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		6 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
	}
}

eastern_thalassocracy = {
	monarchy = yes
	
	color = { 100 150 80 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	
	ai_will_do = {
		factor = 0
	}
	ai_importance = 0
	
	max_states = 2
	
	rank = {
		1 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		2 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		3 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		4 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		5 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		6 = {
			global_manpower_modifier = 0.1
			global_regiment_cost = -0.10
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
	}
}

indian_monarchy = {
	monarchy = yes
	
	color = { 175 30 175 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	allow_vassal_alliance = yes
	
	max_states = 2
	
	rank = {
		1 = {
			global_trade_goods_size_modifier = 0.1
			global_regiment_cost = -0.05
			liberty_desire = -10
			
			global_autonomy = 0.15
			max_absolutism = -5
		}
		2 = {
			global_trade_goods_size_modifier = 0.1
			global_regiment_cost = -0.05
			liberty_desire = -10
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		3 = {
			global_trade_goods_size_modifier = 0.1
			global_regiment_cost = -0.05
			liberty_desire = -10
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		4 = {
			global_trade_goods_size_modifier = 0.1
			global_regiment_cost = -0.05
			liberty_desire = -10
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		5 = {
			global_trade_goods_size_modifier = 0.1
			global_regiment_cost = -0.05
			liberty_desire = -10
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		6 = {
			global_trade_goods_size_modifier = 0.1
			global_regiment_cost = -0.05
			liberty_desire = -10
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
	}
}

rajput_monarchy = {
	monarchy = yes
	tribal = yes
	
	color = { 175 175 30 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_vassal_alliance = yes
	allow_convert = no
	
	ai_will_do = {
		factor = 0
	}
	
	rank = {
		1 = {
			global_manpower_modifier = 0.1
			land_maintenance_modifier = -0.10
			naval_maintenance_modifier = 0.10
			land_forcelimit_modifier = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -5
		}
		2 = {
			global_manpower_modifier = 0.1
			land_maintenance_modifier = -0.10
			naval_maintenance_modifier = 0.10
			land_forcelimit_modifier = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -5
		}
		3 = {
			global_manpower_modifier = 0.1
			land_maintenance_modifier = -0.10
			naval_maintenance_modifier = 0.10
			land_forcelimit_modifier = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -5
		}
		4 = {
			global_manpower_modifier = 0.1
			land_maintenance_modifier = -0.10
			naval_maintenance_modifier = 0.10
			land_forcelimit_modifier = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -5
		}
		5 = {
			global_manpower_modifier = 0.1
			land_maintenance_modifier = -0.10
			naval_maintenance_modifier = 0.10
			land_forcelimit_modifier = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -5
		}
		6 = {
			global_manpower_modifier = 0.1
			land_maintenance_modifier = -0.10
			naval_maintenance_modifier = 0.10
			land_forcelimit_modifier = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -5
		}
	}
}

maratha_confederacy = {
	monarchy = yes
	
	color = { 210 120 210 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	allow_vassal_alliance = yes
	
	max_states = 2
	
	rank = {
		1 = {
			global_unrest = -2
			global_regiment_cost = -0.1
			
			global_autonomy = 0.1
			max_absolutism = -15
		}
		2 = {
			global_unrest = -2
			global_regiment_cost = -0.1
			
			global_autonomy = 0.1
			max_absolutism = -15
		}
		3 = {
			global_unrest = -2
			global_regiment_cost = -0.1
			
			global_autonomy = 0.1
			max_absolutism = -15
		}
		4 = {
			global_unrest = -2
			global_regiment_cost = -0.1
			
			global_autonomy = 0.1
			max_absolutism = -15
		}
		5 = {
			global_unrest = -2
			global_regiment_cost = -0.1
			
			global_autonomy = 0.1
			max_absolutism = -15
		}
		6 = {
			global_unrest = -2
			global_regiment_cost = -0.1
			
			global_autonomy = 0.1
			max_absolutism = -15
		}
	}
	fixed_rank = 5
}

# scholar government
chinese_monarchy = {
	monarchy = yes
	
	color = { 125 30 30 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 4
	
	
	rank = {
		1 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.20
			global_unrest = -2
			adm_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.06
			max_absolutism = -5
		}
		2 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.20
			global_unrest = -2
			adm_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.06
			max_absolutism = -5
		}
		3 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.20
			global_unrest = -2
			adm_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.06
			max_absolutism = -5
		}
		4 = {	#Gong
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.20
			global_unrest = -2
			adm_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.06
			max_absolutism = -5
		}
		5 = {	#Wang
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.20
			global_unrest = -2
			adm_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.06
			max_absolutism = -5
		}
		6 = {	#Huangdi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.20
			global_unrest = -2
			adm_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.06
			max_absolutism = -5
		}
	}
}

chinese_horde_1 = {
	monarchy = yes
	nomad = yes
	
	color = { 197 100 62 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 4
	
	rank = {
		1 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			cav_to_inf_ratio = 0.25
			land_attrition = -0.25
			leader_land_manuever = 1
			
			global_autonomy = 0.05
			max_absolutism = -5
		}
		2 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			cav_to_inf_ratio = 0.25
			land_attrition = -0.25
			leader_land_manuever = 1
			
			global_autonomy = 0.05
			max_absolutism = -5
		}
		3 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			cav_to_inf_ratio = 0.25
			land_attrition = -0.25
			leader_land_manuever = 1
			
			global_autonomy = 0.05
			max_absolutism = -5
		}
		4 = {	#Gong
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			cav_to_inf_ratio = 0.25
			land_attrition = -0.25
			leader_land_manuever = 1
			
			global_autonomy = 0.05
			max_absolutism = -5
		}
		5 = {	#Wang
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			cav_to_inf_ratio = 0.25
			land_attrition = -0.25
			leader_land_manuever = 1
			
			global_autonomy = 0.05
			max_absolutism = -5
		}
		6 = {	#Huangdi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			cav_to_inf_ratio = 0.25
			land_attrition = -0.25
			leader_land_manuever = 1
			
			global_autonomy = 0.05
			max_absolutism = -5
		}
	}
}

# aristocratic government
chinese_monarchy_2 = {
	monarchy = yes
	
	color = { 175 30 30 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 4
	
	rank = {
		1 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.0
			max_absolutism = 0
		}
		2 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.0
			max_absolutism = 0
		}
		3 = {	#Tusi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.0
			max_absolutism = 0
		}
		4 = {	#Gong
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.0
			max_absolutism = 0
		}
		5 = {	#Wang
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.0
			max_absolutism = 0
		}
		6 = {	#Huangdi
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.10
			global_manpower_modifier = 0.1
			mil_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.0
			max_absolutism = 0
		}
	}
}

# bureaucratic government
chinese_monarchy_3 = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 5
	
	rank = {
		1 = {
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.15
			stability_cost_modifier = -0.10
			technology_cost = 0.10
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.05
			max_absolutism = 10
		}
		2 = {
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.15
			stability_cost_modifier = -0.10
			technology_cost = 0.10
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.05
			max_absolutism = 10
		}
		3 = {
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.15
			stability_cost_modifier = -0.10
			technology_cost = 0.10
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.05
			max_absolutism = 10
		}
		4 = {
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.15
			stability_cost_modifier = -0.10
			technology_cost = 0.10
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.05
			max_absolutism = 10
		}
		5 = {
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.15
			stability_cost_modifier = -0.10
			technology_cost = 0.10
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.05
			max_absolutism = 10
		}
		6 = {
			state_maintenance_modifier = 0.15 #global_tax_modifier = -0.15
			stability_cost_modifier = -0.10
			technology_cost = 0.10
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = -0.05
			max_absolutism = 10
		}
	}
}

# theocratic government
chinese_monarchy_4 = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 4
	
	rank = {
		1 = {
			global_manpower_modifier = -0.15
			dip_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			tolerance_own = 1
			tolerance_heathen = -1
			tolerance_heretic = -1
			
			global_autonomy = 0.05
		}
		2 = {
			global_manpower_modifier = -0.15
			dip_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			tolerance_own = 1
			tolerance_heathen = -1
			tolerance_heretic = -1
			
			global_autonomy = 0.05
		}
		3 = {
			global_manpower_modifier = -0.15
			dip_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			tolerance_own = 1
			tolerance_heathen = -1
			tolerance_heretic = -1
			
			global_autonomy = 0.05
		}
		4 = {
			global_manpower_modifier = -0.15
			dip_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			tolerance_own = 1
			tolerance_heathen = -1
			tolerance_heretic = -1
			
			global_autonomy = 0.05
		}
		5 = {
			global_manpower_modifier = -0.15
			dip_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			tolerance_own = 1
			tolerance_heathen = -1
			tolerance_heretic = -1
			
			global_autonomy = 0.05
		}
		6 = {
			global_manpower_modifier = -0.15
			dip_tech_cost_modifier = -0.05
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			tolerance_own = 1
			tolerance_heathen = -1
			tolerance_heretic = -1
			
			global_autonomy = 0.05
		}
	}
}

# peasant government
chinese_monarchy_5 = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 3
	
	rank = {
		1 = {
			global_trade_goods_size_modifier = 0.1
			land_morale = 0.1
			global_unrest = -2
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		2 = {
			global_trade_goods_size_modifier = 0.1
			land_morale = 0.1
			global_unrest = -2
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		3 = {
			global_trade_goods_size_modifier = 0.1
			land_morale = 0.1
			global_unrest = -2
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		4 = {
			global_trade_goods_size_modifier = 0.1
			land_morale = 0.1
			global_unrest = -2
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		5 = {
			global_trade_goods_size_modifier = 0.1
			land_morale = 0.1
			global_unrest = -2
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
		6 = {
			global_trade_goods_size_modifier = 0.1
			land_morale = 0.1
			global_unrest = -2
			heir_chance = 1.0 #Polygamy meant an endless supply of potential heirs, but also succession crises
			
			global_autonomy = 0.15
			max_absolutism = -10
		}
	}
}

# Celestial Empire (not actually used in M&T, should never appear in-game)
celestial_empire = {
	monarchy = yes
	
	color = { 210 30 30 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	allow_convert = no
	nation_designer_trigger = {
		culture_group = chinese_group
	}
	maintain_dynasty = yes
	
	max_states = 2
	
	rank = {
		1 = {
			
		}
		2 = {
			
		}
		3 = {
			
		}
		4 = {
			
		}
		5 = {
			
		}
		6 = {
			
		}
	}
}

# Special Religious Governments
tibetan_theocracy = {
	religion = yes
	
	color = { 195 195 230 }
	
	valid_for_new_country = no
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
		culture_group = tibetic
	}
	
	royal_marriage = no
	
	ai_will_do = {
		factor = 0
	}
	
	max_states = 2
	
	rank = {
		1 = {
			tolerance_own = 1
			diplomatic_reputation = 1
			prestige = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		2 = {
			tolerance_own = 1
			diplomatic_reputation = 1
			prestige = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		3 = {
			tolerance_own = 1
			diplomatic_reputation = 1
			prestige = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		4 = {
			tolerance_own = 1
			diplomatic_reputation = 1
			prestige = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		5 = {
			tolerance_own = 1
			diplomatic_reputation = 1
			prestige = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
		6 = {
			tolerance_own = 1
			diplomatic_reputation = 1
			prestige = 0.20
			
			global_autonomy = 0.10
			max_absolutism = -10
		}
	}
}
