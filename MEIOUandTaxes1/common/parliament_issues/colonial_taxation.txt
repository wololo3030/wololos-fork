colonial_taxation = {
	
	category = 2
	
	allow = {
		colony = 2
	}
	
	effect = {
		#
	}
	
	modifier = {
		global_tariffs = 0.25
	}
	
	ai_will_do = {
		factor = 1
	}
}