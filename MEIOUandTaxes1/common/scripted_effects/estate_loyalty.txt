estate_loyalty_breakdown = {
	set_variable = { which = estate_$type1$_treasury_annual_1_percent which = estate_$type2$_treasury_annual_1 }
	set_variable = { which = estate_$type1$_treasury_annual_2_percent which = estate_$type2$_treasury_annual_2 }
	set_variable = { which = estate_$type1$_treasury_annual_3_percent which = estate_$type2$_treasury_annual_3 }
	set_variable = { which = estate_$type1$_treasury_annual_4_percent which = estate_$type2$_treasury_annual_4 }
	set_variable = { which = estate_$type1$_treasury_annual_5_percent which = estate_$type2$_treasury_annual_5 }
	set_variable = { which = estate_$type1$_weight_share_avg_percent which = estate_$type2$_weight_share_avg }
	set_variable = { which = estate_$type1$_weight_share_1_percent which = estate_$type2$_weight_share_1 }
	set_variable = { which = estate_$type1$_weight_share_2_percent which = estate_$type2$_weight_share_2 }
	set_variable = { which = estate_$type1$_weight_share_3_percent which = estate_$type2$_weight_share_3 }
	set_variable = { which = estate_$type1$_weight_share_4_percent which = estate_$type2$_weight_share_4 }
	set_variable = { which = estate_$type1$_weight_share_5_percent which = estate_$type2$_weight_share_5 }
	
	multiply_variable = { which = estate_$type1$_treasury_annual_1_percent value = 5 }
	multiply_variable = { which = estate_$type1$_treasury_annual_2_percent value = 5 }
	multiply_variable = { which = estate_$type1$_treasury_annual_3_percent value = 5 }
	multiply_variable = { which = estate_$type1$_treasury_annual_4_percent value = 5 }
	multiply_variable = { which = estate_$type1$_treasury_annual_5_percent value = 5 }
	multiply_variable = { which = estate_$type1$_weight_share_avg_percent value = 100 }
	multiply_variable = { which = estate_$type1$_weight_share_1_percent value = 500 }
	multiply_variable = { which = estate_$type1$_weight_share_2_percent value = 500 }
	multiply_variable = { which = estate_$type1$_weight_share_3_percent value = 500 }
	multiply_variable = { which = estate_$type1$_weight_share_4_percent value = 500 }
	multiply_variable = { which = estate_$type1$_weight_share_5_percent value = 500 }
}

estate_loyalty_breakdown_clear = {
	set_variable = { which = estate_$type$_treasury_annual_1_percent value = 0 }
	set_variable = { which = estate_$type$_treasury_annual_2_percent value = 0 }
	set_variable = { which = estate_$type$_treasury_annual_3_percent value = 0 }
	set_variable = { which = estate_$type$_treasury_annual_4_percent value = 0 }
	set_variable = { which = estate_$type$_treasury_annual_5_percent value = 0 }
	set_variable = { which = estate_$type$_weight_share_avg_percent value = 0 }
	set_variable = { which = estate_$type$_weight_share_1_percent value = 0 }
	set_variable = { which = estate_$type$_weight_share_2_percent value = 0 }
	set_variable = { which = estate_$type$_weight_share_3_percent value = 0 }
	set_variable = { which = estate_$type$_weight_share_4_percent value = 0 }
	set_variable = { which = estate_$type$_weight_share_5_percent value = 0 }
	
	set_variable = { which = estate_influencechange_$type$_prov value = 0 }
}