plague_spread_effect = {
	if = {
		limit = {
			has_province_flag = plague_attacked
			NOT = { has_province_flag = plague_load_shot }
			OR = {
				base_tax = 1
				base_production = 1
			}
			#any_neighbor_province = {
			#	NOT = { has_province_flag = plague_attacked }
			#}
		}
		every_neighbor_province = {
			limit = {
				NOT = { has_province_flag = plague_attacked }
				OR = {
					NOT = { has_province_flag = plague_attacked_set }
					NOT = { check_variable = { which = plague_strength which = PREV } }
				}
			}
			set_variable = { which = plague_strength value = 0 }
			set_variable = { which = plague_strength which = PREV }
			set_province_flag = plague_attacked_set
		}
		if = {
			limit = {
				has_province_flag = plague_port_potential
				sea_zone = {
					OR = {
						NOT = { has_province_flag = plague_infected_waters }
						NOT = { check_variable = { which = plague_strength which = PREV } }
					}
				}
			}
			sea_zone = {
				set_province_flag = plague_infected_waters
				set_variable = { which = plague_strength value = 0 }
				set_variable = { which = plague_strength which = PREV }
			}
		}
		set_province_flag = plague_load_shot
	}
}



plague_spread_port_effect = {
	if = {
		limit = {
			NOT = { has_province_flag = plague_attacked }
			has_province_flag = plague_port_potential
			sea_zone = {
				has_province_flag = plague_infected_waters
			}
		}
		set_variable = { which = plague_strength value = 0 }
		set_province_flag = plague_attacked_set
		sea_zone = {
			PREV = { set_variable = { which = plague_strength which = PREV } }
		}
	}
}

plague_calculate_province_strength_seed = {
	set_province_flag = plague_attacked_set
	random_list = {
		10 = { set_variable = { which = plague_strength value = 130 } }
		15 = { set_variable = { which = plague_strength value = 120 } }
		25 = { set_variable = { which = plague_strength value = 110 } }
		25 = { set_variable = { which = plague_strength value = 100 } }
		15 = { set_variable = { which = plague_strength value = 90 } }
		10 = { set_variable = { which = plague_strength value = 80 } }
	}
	set_variable = { which = plague_strength_local which = plague_strength }
	
	multiply_variable = { which = plague_strength_local which = plague_vulnerability }
	multiply_variable = { which = plague_strength_local which = plague_population }
	multiply_variable = { which = plague_strength_local which = plague_time_diminish }
	
	change_variable = { which = plague_strength which = plague_strength_local }
	divide_variable = { which = plague_strength value = 2 } # Average
	if = {
		limit = {
			check_variable = { which = plague_strength value = 200 }
		}
		set_variable = { which = plague_strength value = 200 }
	}
}

plague_calculate_province_strength = {
	random_list = {
		10 = { set_variable = { which = plague_random_spread_multiplier value = 1.00 } }
		15 = { set_variable = { which = plague_random_spread_multiplier value = 0.90 } }
		15 = { set_variable = { which = plague_random_spread_multiplier value = 0.80 } }
		20 = { set_variable = { which = plague_random_spread_multiplier value = 0.70 } }
		15 = { set_variable = { which = plague_random_spread_multiplier value = 0.55 } }
		15 = { set_variable = { which = plague_random_spread_multiplier value = 0.40 } }
		10 = { set_variable = { which = plague_random_spread_multiplier value = 0.25 } }
	}
	
	multiply_variable = { which = plague_vulnerability which = plague_population } # Does this here for flair-up calc purposes
	if = {
		limit = {
			NOT = { check_variable = { which = plague_strength value = 150 } }
		}
		if = {
			limit = {
				check_variable = { which = plague_vulnerability value = 0.95 }
				NOT = { check_variable = { which = plague_vulnerability value = 1.10 } }
			}
			random_list = {
				5 = { multiply_variable = { which = plague_random_spread_multiplier value = 1.75 } set_province_flag = plague_flairup_debug_remove_later }
				95 = { }
			}
		}
		if = {
			limit = {
				check_variable = { which = plague_vulnerability value = 1.10 }
				NOT = { check_variable = { which = plague_vulnerability value = 1.25 } }
			}
			random_list = {
				5 = { multiply_variable = { which = plague_random_spread_multiplier value = 2.00 } set_province_flag = plague_flairup_debug_remove_later }
				5 = { multiply_variable = { which = plague_random_spread_multiplier value = 1.50 } set_province_flag = plague_flairup_debug_remove_later }
				90 = { }
			}
		}
		if = {
			limit = {
				check_variable = { which = plague_vulnerability value = 1.25 }
			}
			random_list = {
				5 = { multiply_variable = { which = plague_random_spread_multiplier value = 2.00 } set_province_flag = plague_flairup_debug_remove_later }
				5 = { multiply_variable = { which = plague_random_spread_multiplier value = 1.75 } set_province_flag = plague_flairup_debug_remove_later }
				5 = { multiply_variable = { which = plague_random_spread_multiplier value = 1.50 } set_province_flag = plague_flairup_debug_remove_later }
				85 = { }
			}
		}
	}
	
	set_variable = { which = plague_strength_local which = plague_strength }
	
	multiply_variable = { which = plague_strength_local which = plague_random_spread_multiplier }
	multiply_variable = { which = plague_strength_local which = plague_vulnerability }
	#multiply_variable = { which = plague_strength_local which = plague_population }
	multiply_variable = { which = plague_strength_local which = plague_time_diminish }
	
	change_variable = { which = plague_strength which = plague_strength_local }
	divide_variable = { which = plague_strength value = 2 } # Average
}

plague_remove_plague_modifier = {
	trigger_switch = {
		on_trigger = has_province_modifier
		plague_devastating = {
			remove_province_modifier = plague_devastating
		}
		plague_terrible = {
			remove_province_modifier = plague_terrible
		}
		plague_severe = {
			remove_province_modifier = plague_severe
		}
		plague_moderate = {
			remove_province_modifier = plague_moderate
		}
		plague_mild = {
			remove_province_modifier = plague_mild
		}
		plague_passed_over = {
			remove_province_modifier = plague_passed_over
		}
	}
}

plague_assign_plague_modifier = {
	if = {
		limit = {
			check_variable = { which = plague_strength_local value = 150 }
		}
		if = {
			limit = {
				NOT = { has_province_modifier = plague_devastating }
			}
			plague_remove_plague_modifier = yes
			add_permanent_province_modifier = {
				name = plague_devastating
				duration = 900
			}
		}
		set_province_flag = plague_attacked
	}
	else_if = {
		limit = {
			check_variable = { which = plague_strength_local value = 100 }
		}
		if = {
			limit = {
				NOT = { has_province_modifier = plague_terrible }
			}
			plague_remove_plague_modifier = yes
			add_permanent_province_modifier = {
				name = plague_terrible
				duration = 900
			}
		}
		set_province_flag = plague_attacked
	}
	else_if = {
		limit = {
			check_variable = { which = plague_strength_local value = 60 }
		}
		if = {
			limit = {
				NOT = { has_province_modifier = plague_severe }
			}
			plague_remove_plague_modifier = yes
			add_permanent_province_modifier = {
				name = plague_severe
				duration = 900
			}
		}
		set_province_flag = plague_attacked
	}
	else_if = {
		limit = {
			check_variable = { which = plague_strength_local value = 25 }
		}
		if = {
			limit = {
				NOT = { has_province_modifier = plague_moderate }
			}
			plague_remove_plague_modifier = yes
			add_permanent_province_modifier = {
				name = plague_moderate
				duration = 900
			}
		}
		set_province_flag = plague_attacked
	}
	else_if = {
		limit = {
			OR = {
				check_variable = { which = plague_strength_local value = 15 }
				check_variable = { which = plague_strength value = 15 }
			}
		}
		if = {
			limit = {
				NOT = { has_province_modifier = plague_mild }
			}
			plague_remove_plague_modifier = yes
			add_permanent_province_modifier = {
				name = plague_mild
				duration = 900
			}
		}
		set_province_flag = plague_attacked
	}
	else_if = {
		limit = {
			check_variable = { which = plague_strength value = 0 }
			NOT = {
				OR = {
					check_variable = { which = plague_strength_local value = 15 }
					check_variable = { which = plague_strength value = 15 }
				}
			}
		}
		if = {
			limit = {
				NOT = { has_province_modifier = plague_passed_over }
			}
			plague_remove_plague_modifier = yes
			add_permanent_province_modifier = {
				name = plague_passed_over
				duration = 900
			}
		}
		clr_province_flag = plague_attacked_set
	}
	else = {
		plague_remove_plague_modifier = yes
	}
}

plague_trade_center_spread = {
	set_variable = { which = plague_strength_dormant value = 0 }
	set_variable = { which = plague_strength_dormant which = plague_strength }
	if = {
		limit = {
			continent = europe
		}
		if = {
			limit = {
				has_province_modifier = minor_center_of_trade
			}
			random_list = {
				50 = { }
				50 = {
					random_province = {
						limit = {
							continent = europe
							OR = {
								has_province_modifier = major_center_of_trade
								has_province_modifier = dominant_center_of_trade
								has_province_modifier = important_center_of_trade
								has_province_modifier = minor_center_of_trade
							}
						}
						if = {
							limit = {
								owner = {
									has_country_flag = trade_restricted
								}
							}
							random_list = {
								50 = { set_province_flag = plague_evaded }
								50 = { }
							}
						}
						if = {
							limit = {
								OR = {
									NOT = { has_province_flag = plague_attacked }
									NOT = { has_province_flag = plague_attacked_set }
									NOT = { check_variable = { which = plague_strength_dormant value = 0.01 } }
								}
								NOT = { has_province_flag = plague_evaded }
							}
							set_variable = { which = plague_strength_dormant value = 0 }
							set_variable = { which = plague_strength_dormant which = PREV }
							if = {
								limit = {
									PREV = { has_province_modifier = minor_center_of_trade }
								}
								multiply_variable = { which = plague_strength_dormant value = 0.75 }
							}
							province_event = {
								id = POP_Plague.020
								days = 90
							}
						}
					}
				}
			}
		}
		if = {
			limit = {
				OR = {
					has_province_modifier = major_center_of_trade
					has_province_modifier = dominant_center_of_trade
					has_province_modifier = important_center_of_trade
				}
			}
			random_province = {
				limit = {
					continent = europe
					OR = {
						has_province_modifier = major_center_of_trade
						has_province_modifier = dominant_center_of_trade
						has_province_modifier = important_center_of_trade
						has_province_modifier = minor_center_of_trade
					}
				}
				if = {
					limit = {
						owner = {
							has_country_flag = trade_restricted
						}
					}
					random_list = {
						50 = { set_province_flag = plague_evaded }
						50 = { }
					}
				}
				if = {
					limit = {
						OR = {
							NOT = { has_province_flag = plague_attacked }
							NOT = { has_province_flag = plague_attacked_set }
							NOT = { check_variable = { which = plague_strength_dormant value = 0.01 } }
						}
						NOT = { has_province_flag = plague_evaded }
					}
					set_variable = { which = plague_strength_dormant value = 0 }
					set_variable = { which = plague_strength_dormant which = PREV }
					if = {
						limit = {
							PREV = { has_province_modifier = minor_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 0.7 }
					}
					if = {
						limit = {
							PREV = { has_province_modifier = important_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 0.8 }
					}
					if = {
						limit = {
							PREV = { has_province_modifier = major_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 0.9 }
					}
					if = {
						limit = {
							PREV = { has_province_modifier = dominant_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 1 }
					}
					province_event = {
						id = POP_Plague.020
						days = 90
					}
				}
			}
		}
	}
	if = {
		limit = {
			continent = mena
		}
		if = {
			limit = {
				has_province_modifier = minor_center_of_trade
			}
			random_list = {
				50 = { }
				50 = {
					random_province = {
						limit = {
							continent = mena
							OR = {
								has_province_modifier = major_center_of_trade
								has_province_modifier = dominant_center_of_trade
								has_province_modifier = important_center_of_trade
								has_province_modifier = minor_center_of_trade
							}
						}
						if = {
							limit = {
								owner = {
									has_country_flag = trade_restricted
								}
							}
							random_list = {
								50 = { set_province_flag = plague_evaded }
								50 = { }
							}
						}
						if = {
							limit = {
								OR = {
									NOT = { has_province_flag = plague_attacked }
									NOT = { has_province_flag = plague_attacked_set }
									NOT = { check_variable = { which = plague_strength_dormant value = 0.01 } }
								}
								NOT = { has_province_flag = plague_evaded }
							}
							set_variable = { which = plague_strength_dormant value = 0 }
							set_variable = { which = plague_strength_dormant which = PREV }
							if = {
								limit = {
									PREV = { has_province_modifier = minor_center_of_trade }
								}
								multiply_variable = { which = plague_strength_dormant value = 0.75 }
							}
							province_event = {
								id = POP_Plague.020
								days = 90
							}
						}
					}
				}
			}
		}
		if = {
			limit = {
				OR = {
					has_province_modifier = major_center_of_trade
					has_province_modifier = dominant_center_of_trade
					has_province_modifier = important_center_of_trade
				}
			}
			random_province = {
				limit = {
					continent = mena
					OR = {
						has_province_modifier = major_center_of_trade
						has_province_modifier = dominant_center_of_trade
						has_province_modifier = important_center_of_trade
						has_province_modifier = minor_center_of_trade
					}
				}
				if = {
					limit = {
						owner = {
							has_country_flag = trade_restricted
						}
					}
					random_list = {
						50 = { set_province_flag = plague_evaded }
						50 = { }
					}
				}
				if = {
					limit = {
						OR = {
							NOT = { has_province_flag = plague_attacked }
							NOT = { has_province_flag = plague_attacked_set }
							NOT = { check_variable = { which = plague_strength_dormant value = 0.01 } }
						}
						NOT = { has_province_flag = plague_evaded }
					}
					set_variable = { which = plague_strength_dormant value = 0 }
					set_variable = { which = plague_strength_dormant which = PREV }
					if = {
						limit = {
							PREV = { has_province_modifier = minor_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 0.7 }
					}
					if = {
						limit = {
							PREV = { has_province_modifier = important_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 0.8 }
					}
					if = {
						limit = {
							PREV = { has_province_modifier = major_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 0.9 }
					}
					if = {
						limit = {
							PREV = { has_province_modifier = dominant_center_of_trade }
						}
						multiply_variable = { which = plague_strength_dormant value = 1 }
					}
					province_event = {
						id = POP_Plague.020
						days = 90
					}
				}
			}
		}
	}
}
