#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 5 73 12 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Agbarjin" = 20
	"Adai" = 20
	"Altan" = 20
	"Arigaba" = 10
	"Arugtai" = 10
	"Ayurparibhadra" = 10
	"Bodi" = 20
	"Bek" = 20
	"Bahamu" = 20
	"Bars" = 20
	"Buyan" = 20
	"Biligt� " = 10
	"Dayan" = 20
	"Darayisung" = 20
	"Delbeg" = 20
	"Ejei" = 10
	"Elbeg" = 10
	"Engke" = 10
	"G�y�k" = 10
	"G�n" = 20
	"Gulichi" = 20
	"Irinchibal" = 10
	"Jorightu" = 10
	"Kublai" = 10
	"K�ke" = 10
	"Ligdan" = 20
	"Mark�rgis" = 10
	"M�ngke" = 10
	"Mulan" = 20
	"Manduulun" = 20
	"�ljei" = 20
	"�r�g" = 15
	"Oyiradai" = 20
	"�g�dei" = 10
	"Qayshan" = 10
	"Qoshila" = 10
	"Sayn" = 20
	"Suddhipala" = 10
	"Tayisung" = 20
	"Temujin" = 10
	"Tem�r" = 10
	"Toghun" = 10
	"T�men" = 10
	"Tolui" = 10
	"Toq" = 10
	"Uskhal" = 10
	"Yes�n" = 10
	"Ariq" = 1
	"Batu" = 1
	"Eljigidei" = 1
	"Joichi" = 1
	
	"Heying" = -1
	"Chunmei" = -1
	"Tianhui" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
}

leader_names = {
	Khori Barga Buryaad Ekhired Bulagad Sartuul Bulagad Khongoodor
}

ship_names = {
	Yingchang Karakorum Hohhot Chagaan
	"Ghengis Khan" J�chi �g�dei Tolui
	M�ngke Qubilai H�leg� B�ke
	Jinggim Kamala Dharmapala "�r�g Tem�r"
	Adai Elbeg Batu Mugali "Bo'orchu" Borokhula Chilaun
	Khubilai Jelme Jebe Subutai
	"Yel� Chucai" Shikhikhutug "Sorqan Shira" "Khar Khiruge"
	"D�rben K�l�'�d" "D�rben Noyas"
}
