magnate_estate_outside_trigger = {
	OR = {
		has_province_modifier = bohemian_estates
		has_province_modifier = hungarian_estates
		has_province_modifier = polish_estates
		has_province_modifier = lithuanian_estates
	}
}

estate_church_trigger = {
	NOT = { government = merchant_republic }
	NOT = { government = trade_company_gov }
	NOT = { government = venetian_republic }
	NOT = { government = siena_republic }
	NOT = { government = oligarchic_republic }
	NOT = { government = native_council }
	NOT = { government = trading_city }
	NOT = { government = siberian_native_council }
	NOT = { religion = confucianism }
	NOT = { religion = tengri_pagan_reformed }
	NOT = { religion = animism }
	
	OR = {
		NOT = { tag = USA }
		government = theocracy
	}
	
	is_nomad = no
	is_colonial_nation = no
}

estate_greater_nobles_trigger = {
	always = yes
	# NOT = { government = merchant_republic }
	# NOT = { government = venetian_republic }
	# NOT = { government = siena_republic }
	# NOT = { government = oligarchic_republic }
	# NOT = { government = native_council }
	# NOT = { government = siberian_native_council }
	# NOT = { government = revolutionary_republic }
	# NOT = { government = revolutionary_empire }
	# is_nomad = no
	# is_colonial_nation = no
	# NOT = { government = chinese_monarchy_3 }
	# NOT = { government = chinese_monarchy_2 }
	# NOT = { government = chinese_monarchy_4 }
	# NOT = { government = chinese_monarchy }
}

estate_city_burghers_trigger = {
	always = yes
}

estate_cossacks_trigger = {
	religion_group = christian
	
	any_owned_province = {
		OR = {
			superregion = greater_russia_superregion
			region = ruthenia_region
		}
	}
	NOT = { government = merchant_republic }
	NOT = { government = trading_city }
	NOT = { government = revolutionary_republic }
	NOT = { government = native_council }
	NOT = { government = siberian_native_council }
	is_nomad = no
	is_colonial_nation = no
	NOT = { government = celestial_empire }
}

estate_nomadic_tribes_trigger = {
	always = yes
}

#estate_dhimmi_trigger = {
#	NOT = { government = merchant_republic }
#	NOT = { government = trading_city }
#	NOT = { government = revolutionary_republic }
#	NOT = { government = native_council }
#	NOT = { government = siberian_native_council }
#	is_nomad = no
#	is_colonial_nation = no
#	NOT = { government = celestial_empire }
#	NOT = { technology_group = western }
#	religion_group = muslim
#
#	NOT = {
#		capital_scope = { continent = indian_continent }
#	}
#
#	check_variable = {
#		which = jizya_population
#		value = 0.1
#	}
#}
#
#estate_magnates_trigger = {
#	OR = {
#		government = elective_monarchy
#		government = noble_republic
#		government = trading_city
#
#		AND = {
#			has_country_flag = king_of_bohemia_outside
#			NOT = { has_country_flag = bohemian_magnetes_broken }
#		}
#
#		AND = {
#			has_country_flag = king_of_hungary_outside
#			NOT = { has_country_flag = hungarian_magnetes_broken }
#		}
#
#		AND = {
#			has_country_flag = king_of_poland_outside
#			NOT = { has_country_flag = polish_magnetes_broken }
#		}
#
#		has_country_flag = kingdom_of_poland
#		has_country_flag = grand_duke_of_lithuania
#	}
#}
#
#estate_weisuo_trigger = {
#	has_country_modifier = wei_suo_system
#}

estate_lesser_nobles_trigger = {
	always = yes
}

estate_bureaucracy_trigger = {
	always = yes
}
