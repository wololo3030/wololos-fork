#34 - Carabiners

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 6
defensive_morale = 4
offensive_fire = 4
defensive_fire = 2
offensive_shock = 3
defensive_shock = 4

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
