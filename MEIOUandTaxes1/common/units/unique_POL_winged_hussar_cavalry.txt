#31 - Heavy Hussars

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 4
defensive_morale = 5
offensive_fire = 0
defensive_fire = 1
offensive_shock = 7 #BONUS
defensive_shock = 5

trigger = {
	primary_culture = polish #Winged Hussars
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}


