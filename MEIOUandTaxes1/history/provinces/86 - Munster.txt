
owner = MUN
controller = MUN
add_core = MUN

capital = "M�nster"
trade_goods = wool
culture = westphalian
religion = catholic

hre = yes

base_tax = 9
base_production = 1
base_manpower = 1

is_city = yes
workshop = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1169.1.1 = {
	add_permanent_province_modifier = {
		name = "herrschaft_anholt"
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 12
	base_production = 0
	base_manpower = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1534.5.1 = {
	unrest = 3
} # The Babtists (T�ufer) are executed and removed from power during 1534 and 1535
1535.3.1 = {
	unrest = 0
}
1585.1.1 = {
	owner = KOL
	controller = KOL
	add_core = KOL
}
1643.1.1 = { } # The 30 years war was hard on M�nster. It is one of the two towns where the peace is negotiated. 1643-1648
1650.1.1 = {
	owner = MUN
	controller = MUN
	remove_core = KOL
} # M�nster's Bishop is no longer the Bishop of K�ln
1723.1.1 = {
	owner = KOL
	controller = KOL
	add_core = KOL
	remove_core = MUN
} # Max-Clemens-Kanal increases trade, but does not reach its complete potential. Clemens August also becomes Bishop of K�ln
1773.1.1 = {
	early_modern_university = yes
} # University founded in M�nster
1802.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
} # Ceded to Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.1 = {
	controller = FRA
} # Controlled by France
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # Treaty of Paris
