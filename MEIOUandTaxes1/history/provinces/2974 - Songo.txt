# 2974 - Songo

owner = KON
controller = KON
add_core = KON

capital = "Mbanza Kongo"
trade_goods = millet
culture = kongolese
religion = animism

hre = no

base_tax = 19
base_production = 2
base_manpower = 1

is_city = yes
town_hall = yes
marketplace = yes
workshop = yes

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
}
1483.1.1 = {
	discovered_by = POR
} # Diogo C�o
1506.6.2 = {
	religion = catholic
}
1520.1.1 = {
	base_tax = 15
}