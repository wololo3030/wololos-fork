# 973 - Niederstift
# GG - 22/07/2008

owner = MUN
controller = MUN
add_core = MUN

capital = "Meppen"
trade_goods = wool
culture = westphalian
religion = catholic

hre = yes

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 8
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1585.1.1 = {
	owner = KOL
	controller = KOL
	add_core = KOL
}
1650.1.1 = {
	owner = MUN
	controller = MUN
	remove_core = KOL
} # M�nster's Bishop is no longer the Bishop of K�ln
1723.1.1 = {
	owner = KOL
	controller = KOL
	add_core = KOL
	remove_core = MUN
}
1802.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
} # Ceded to Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.1 = {
	controller = FRA
} # Controlled by France
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = FRA
} # Treaty of Paris
