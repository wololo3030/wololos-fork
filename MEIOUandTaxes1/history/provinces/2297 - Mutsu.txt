# 2297 - Aomori
# GG/LS - Japanese Civil War

owner = NNB
controller = NNB
add_core = NNB

capital = "Sannofe"
trade_goods = fish
culture = tohoku
religion = mahayana #shinbutsu

hre = no

base_tax = 15
base_production = 0
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 25
	base_production = 1
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1582.1.1 = {
	owner = DTE
	controller = DTE
}
1630.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
