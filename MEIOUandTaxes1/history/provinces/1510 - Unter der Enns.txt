#1510 - Niederostarrichi

owner = HAB
controller = HAB
add_core = HAB

capital = "Enns"
trade_goods = wheat
culture = austrian
religion = catholic

hre = yes

base_tax = 12
base_production = 0
base_manpower = 1

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1520.5.5 = {
	base_tax = 25
	base_production = 1
	base_manpower = 1
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
