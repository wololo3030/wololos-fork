# 2390 - Slavonija

owner = HUN
controller = HUN
add_core = HUN

capital = "Osijek"
trade_goods = wine
culture = croatian
religion = catholic

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
road_network = yes
town_hall = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = CRO
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
	add_permanent_province_modifier = {
		name = slavonia_province
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 8
	base_production = 1
	base_manpower = 1
}
1526.8.30 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_permanent_claim = HAB
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1671.1.1 = {
	unrest = 5
} # Conspiracy against Habsburg rule uncovered
1671.5.1 = {
	unrest = 0
} # Estimated, the conspirators are executed
1699.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
	unrest = 0
} # Austrian occupation
1700.1.1 = {
	unrest = 7
}
