# 1252 - Holstein

owner = SHL
controller = SHL
add_core = SHL

capital = "Kyle"
trade_goods = wax
culture = old_saxon
religion = catholic

hre = yes

base_tax = 8
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "holstein_natural_harbour"
		duration = -1
	}
}
1460.1.1 = {
	add_core = DEN
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 17
	base_production = 1
	base_manpower = 1
}
1523.6.21 = {
	#add_core = DAN
	remove_core = DEN
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1536.1.1 = {
	religion = protestant
}
1644.1.12 = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13 = {
	controller = SHL
} #The Peace of Br�msebro
1657.10.23 = {
	controller = SWE
} #Karl X Gustavs First Danish War-Captured by Wrangel
1658.2.26 = {
	controller = SHL
} #The Peace of Roskilde - Duchy fully independent
1720.7.3 = {
	remove_core = DAN
} # Treaty of Frederiksborg
1773.1.1 = {
	add_core = DAN
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
