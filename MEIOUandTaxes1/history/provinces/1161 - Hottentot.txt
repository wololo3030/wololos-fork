# 1161 - Hottentot

capital = "Hotten"
trade_goods = unknown
culture = khoisan
religion = animism

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 0.5
native_hostileness = 0

450.1.1 = {
	set_province_flag = tribals_control_province
}
1488.1.1 = {
	discovered_by = POR
}
