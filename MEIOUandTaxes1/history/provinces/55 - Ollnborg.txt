#

owner = OLD
controller = OLD
add_core = OLD

capital = "Oldenburg"
trade_goods = livestock
culture = old_saxon
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "oldenburg_natural_harbour"
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
}
1522.2.15 = {
	shipyard = yes
}
1530.1.1 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1607.1.1 = {
	fort_14th = yes
}
1667.1.1 = {
	owner = DAN
	controller = DAN
	add_core = DAN
} # United in a Dynastic union with Denmark after Count Anton Guenther's death. Plague
1682.1.1 = {
	fort_14th = yes
}
1773.1.1 = {
	owner = OLD
	controller = OLD
	remove_core = DAN
} # Oldenburg is sold to the house of Holstein-Gottorp
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = WES
} # Annexed by France
1813.1.1 = {
	owner = OLD
	controller = OLD
	add_core = OLD
	remove_core = FRA
} # Control is returned
