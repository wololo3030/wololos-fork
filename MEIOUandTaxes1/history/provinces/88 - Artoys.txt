# 88 - Artoys
# arras, St Omer, Bethune

owner = ARS
controller = ARS
add_core = ARS

capital = "Aro"
trade_goods = wheat #linen
culture = picard
religion = catholic

hre = no

base_tax = 13
base_production = 5
base_manpower = 1

is_city = yes
road_network = yes
urban_infrastructure_2 = yes
art_corporation = yes
corporation_guild = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1329.1.1 = {
	owner = BUR
	controller = BUR
}
1356.1.1 = {
	add_core = FRA
	add_core = BUR
	add_core = FLA
	rename_capital = "Atrecht"
	change_province_name = "Artesi�"
}
1361.11.21 = {
	owner = ARS
	controller = ARS
}
1369.6.19 = {
	owner = BUR
	controller = BUR
}
1444.1.1 = {
	remove_core = FRA
}
1477.1.5 = {
	add_core = FRA
}
1477.1.5 = {
	unrest = 10
} # Death of Charles the Bold
1477.8.18 = {
	unrest = 0
} # Personal Union with HAS (marriage of Mary of Burgondy & Maximmilian of Hasburg)
1482.3.27 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BUR
} # Mary of burgondy dies, Lowlands to Austria # Frederick III dies, Charles VII cedes Artois to Maximilian I von Habsburg
1493.8.19 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Frederick III dies, Charles VII cedes Artois to Maximilian I von Habsburg
1500.1.1 = {
	road_network = yes
}
1515.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 18
	base_production = 4
}
1529.8.3 = {
	remove_core = FRA
} # 'Ladies Peace' (Damesvrede) of Cambrai: France renounces all claims
1530.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1530.1.5 = {
	owner = BUR
	controller = BUR
	add_core = BUR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = BUR
}
1548.6.26 = {
	hre = yes
} # Artois incorporated into the Holy Roman Empire
1556.1.14 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = HAB
}
1559.5.12 = {
	unrest = 3
} # New bishoprics established in the Lowlands create an outrage
1566.8.1 = {
	unrest = 4
} # 'Beeldenstorm' at hand
1566.8.10 = {
	controller = REB
	culture = picard
	rename_capital = "Arras"
	change_province_name = "Artoys"
} # 'Beeldenstorm' also hits parts of Artois
1567.1.8 = {
	controller = SPA
} # Spain is back in control
1569.1.1 = {
	unrest = 7
} # The Duke of Alba reforms the taxation system ('tiende penning')
1570.1.1 = {
	unrest = 11
} # The Duke of Alba reforms the penal system, 'Blood Council' (Bloedraad) established
1577.2.12 = {
	unrest = 5
} # The 'Perpetual Edict' (Eeuwig Edict) is accepted by Don Juan
1579.1.6 = {
	add_core = EBU
	add_core = ARS
	unrest = 0
} # traite d'arras
1630.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1635.1.1 = {
	controller = FRA
} # French troops capture parts of the Southern Lowlands
1648.1.30 = {
	controller = SPA
} # Peace of M�nster/Westphalia
1650.1.1 = {
	add_core = FRA
} # Chambers of Reunion
1658.6.14 = {
	controller = FRA
} # French troops capture most of the area
1659.10.28 = {
	owner = FRA
	remove_core = SPA
	hre = no
} # Peace of the Pyrennees
1670.10.15 = {
	fort_15th = no
	fort_16th = yes
} # Vauban's fort in Lille is finished, state of the art for its time, troops aimed at the Lowlands based there
1750.1.1 = {
	fort_16th = no
	fort_17th = yes
} # Expansion of the forts
