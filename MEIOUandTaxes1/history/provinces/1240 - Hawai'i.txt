# 1240 - Hawai'i
# MEIOU-GG
# Anarchovampire - corrections

owner = HAW
controller = HAW
add_core = HAW

capital = "Kowahu"
trade_goods = fish
culture = hawaian
religion = polynesian_religion

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = hawaii_tech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1555.1.1 = {
	discovered_by = SPA
}
1778.1.1 = {
	discovered_by = GBR
}
#1795.1.1 = {
#	discovered_by = KOH
#	owner = KOH
#	controller = KOH
#	add_core = KOH
#}
#1810.1.1 = {
#	remove_core = HAW #Kaua'i joins Kingdom
#}
1820.1.1 = {
	religion = protestant
}
1898.1.1 = {
	owner = USA
	controller = USA
}
