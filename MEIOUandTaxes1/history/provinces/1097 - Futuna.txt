# 1097 - Futuna

capital = "Futuna"
trade_goods = unknown #fish
culture = polynesian
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 2
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
}
1616.1.1 = {
	discovered_by = NED
} # Discovered by Willem Schouten
