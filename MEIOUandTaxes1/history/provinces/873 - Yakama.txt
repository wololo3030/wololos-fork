# No previous file for Yakama

capital = "Yakama"
trade_goods = unknown
culture = salish
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 4

450.1.1 = {
	set_province_flag = tribals_control_province
}
1811.1.1 = {
	owner = USA
	controller = USA
	citysize = 350
	trade_goods = fur
	religion = protestant
	culture = american
} # Fort Okanogan
1814.12.24 = {
	owner = GBR
	controller = GBR
	citysize = 200
	culture = english
	religion = protestant
} # British control after the War of 1812
1815.1.1 = {
	add_core = GBR
	is_city = yes
}
