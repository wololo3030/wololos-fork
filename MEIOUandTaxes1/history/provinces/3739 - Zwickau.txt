# 3739 - Zwickau

owner = MEI
controller = MEI
add_core = MEI

capital = "Zwickau"
trade_goods = lumber
culture = high_saxon
religion = catholic

hre = yes

base_tax = 4
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1200.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = silver
}
1423.6.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
}#Margraviate of Meissen inherits Saxony and becomes the Elector.
1500.1.1 = {
	road_network = yes
}
1520.12.10 = {
	religion = protestant
	#reformation_center = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1560.1.1 = {
	fort_15th = yes
}
1790.8.1 = {
	unrest = 5
} # Peasant revolt
1791.1.1 = {
	unrest = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
