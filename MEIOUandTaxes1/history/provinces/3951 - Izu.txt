# 3347 - Izu

owner = HKY
controller = HKY
add_core = HKY

capital = "Shimoda"
trade_goods = fish
culture = chubu
religion = mahayana

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1356.1.1 = {
	add_core = KOS
	add_core = USG
	add_core = HJO
	add_nationalism = 10
	set_province_flag = izu_rebellion
}
1359.1.1 = {
	spawn_rebels = {
		type = nationalist_rebels
		size = 8
		leader = "Ujishige"
		friend = JAP
	}
}
1361.1.1 = {
	controller = REB #Hatakeyama surrendered
}
1362.1.1 = {
	owner = KOS
	controller = KOS
	remove_core = HKY
	clr_province_flag = izu_rebellion
} # Held by Kosaka clan of the Izu branch
1369.1.1 = {
	owner = USG
	controller = USG
}
1493.1.1 = {
	add_core = HJO
	owner = HJO
	controller = HJO
} # Hojo Soun gains control of Izu Province
1501.1.1 = {
	base_tax = 9
}
1542.1.1 = {
	discovered_by = POR
}
1600.9.15 = {
	owner = TGW
	controller = TGW
	add_core = TGW
}
1630.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
