#243 - Lincoln

owner = ENG
controller = ENG
add_core = ENG

capital = "Lincoln"
trade_goods = wool
culture = english
religion = catholic

hre = no

base_tax = 9
base_production = 1
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
town_hall = yes
marketplace = yes
local_fortification_1 = yes
temple = yes #Lincoln Cathedral
road_network = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1461.6.1 = {
	unrest = 2
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1470.3.1 = {
	controller = REB
}
1470.10.6 = {
	controller = ENG
} # Readeption of Henry VI
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.5.4 = {
	unrest = 2
} # Murder of Henry VI & Restoration of Edward IV
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1529.2.5 = {
	road_network = no
	paved_road_network = yes
}
1529.6.6 = {
	base_tax = 14
	base_production = 0
	base_manpower = 1
}
1560.1.1 = {
	religion = protestant
} # anglican
1600.1.1 = {
	fort_14th = yes
}
1643.10.15 = {
	controller = REB
} # Estimated
1646.5.5 = {
	controller = ENG
} # End of First English Civil War
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
