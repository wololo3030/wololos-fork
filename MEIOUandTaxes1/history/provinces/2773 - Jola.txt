# 2773 - Jola

capital = "Jola"
trade_goods = slaves # slaves
culture = mandinka
religion = west_african_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 4.5
native_hostileness = 9

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 8 }
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1437.1.1 = {
	discovered_by = POR
} # Cadamosto
1645.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	rename_capital = "Ziguinchor"
	change_province_name = "Casamanša"
	citysize = 200
	trade_goods = slaves
}
1888.4.22 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = POR
} # Aftermath of the Conference of Berlin
