# 2976 - Mpango

owner = KON
controller = KON
add_core = KON

capital = "Mpango"
trade_goods = millet
culture = kongolese
religion = animism

hre = no

base_tax = 28
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
}
1483.1.1 = {
	discovered_by = POR
} # Diogo C�o
1520.1.1 = {
	base_tax = 35
}
