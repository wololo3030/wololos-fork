# 1132 - Katsina

owner = KTS
controller = KTS
add_core = KTS

capital = "Katsina"
trade_goods = cotton
culture = haussa
religion = sunni

hre = no

base_tax = 16
base_production = 1
base_manpower = 0

is_city = yes
urban_infrastructure_1 = yes
marketplace = yes
workshop = yes
fort_14th = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 12 }
	set_variable = { which = settlement_score_progress_preset	value = 47 }
}