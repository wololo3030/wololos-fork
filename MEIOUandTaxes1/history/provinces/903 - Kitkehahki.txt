# No previous file for Kitkehahki

capital = "Kitkehahki"
trade_goods = unknown
culture = pawnee
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 25
native_ferocity = 4
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}
1760.1.1 = {
	owner = PAW
	controller = PAW
	add_core = PAW
	trade_goods = fur
	is_city = yes
} # Great Plain tribes spread over vast territories
