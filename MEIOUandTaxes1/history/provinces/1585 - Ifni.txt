# 1585 - Ifni

owner = SOS
controller = SOS
add_core = SOS

capital = "Sidi Ifni"
trade_goods = wheat
culture = chleuh
religion = sunni

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = muslim
discovered_by = western
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1472.1.1 = {
	owner = SOS
	controller = SOS
	remove_core = FEZ
}
1476.1.1 = {
	owner = CAS
	controller = CAS
	add_core = CAS
	capital = "Santa Cruz de la Mar Peque��a"
	trade_goods = slaves
}
1516.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = CAS
}
1524.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	capital = "Sidi Ifni"
	trade_goods = millet
}
1525.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1530.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SPA
	remove_core = CAS
	remove_core = FEZ
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.8.1 = {
	unrest = 5
} #Death of al-Mansur, long period of instability follows
1613.1.1 = {
	owner = SOS
	controller = SOS
	unrest = 0
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
1672.1.1 = {
	unrest = 4
} # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = {
	unrest = 0
}
