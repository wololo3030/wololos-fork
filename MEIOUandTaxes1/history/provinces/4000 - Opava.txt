# 4000 - Opava

owner = OPA
controller = OPA

capital = "Opava"
trade_goods = hemp
culture = silesian
religion = catholic

hre = yes

base_tax = 13
base_production = 0
base_manpower = 1

is_city = yes
road_network = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1337.1.1 = {
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
	owner = RAO
	controller = RAO
	add_core = RAO
}
1356.1.1 = {
	add_core = OPA
	add_core = BOH
}
1377.1.1 = {
	owner = OPA
	controller = OPA
	remove_core = RAO
}
1419.8.16 = {
#	owner = HUN
#	controller = HUN
	add_core = HUN
}
1437.12.9 = {
#	owner = BOH
#	controller = BOH
	remove_core = HUN
}
1457.1.1 = {
	unrest = 5
} # George of Podiebrand had to secure recognition from the German and Catholic towns
1464.1.1 = {
	unrest = 1
} # The Catholic nobility still undermines the crown.
1471.1.1 = {
	unrest = 0
}
1500.1.1 = {
	road_network = yes
}
1501.1.1 = {
	owner = BOH
	controller = BOH
	add_core = BOH
}
1520.5.5 = {
	base_tax = 18
	base_production = 0
	base_manpower = 1
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1576.1.1 = {
	religion = reformed
}
1618.4.23 = {
	revolt = {
		type = protestant_rebels
		size = 2
	}
	controller = REB
} # Defenestration of Prague
1619.3.1 = {
	revolt = { }
	controller = BOH
#	owner = PAL
#	controller = PAL
	add_core = PAL
}
1620.11.8 = {
#	owner = HAB
#	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
1621.1.1 = { } # ... and let us start this session by executing the most inconvenient nobles....
1627.1.1 = {
	religion = catholic
} # Order from Ferdinand II to reconvert to Catholicism, many Protestant leave the country
1653.1.1 = {
	owner = HAB
	controller = HAB
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
