# 116 - Firenze

owner = FIR
controller = FIR
add_core = FIR

capital = "Fiorenza"
trade_goods = wheat #cloth
culture = tuscan
religion = catholic

hre = yes

base_tax = 12
base_production = 12
base_manpower = 2

is_city = yes
local_fortification_1 = yes
urban_infrastructure_2 = yes
corporation_guild = yes
marketplace = yes
art_corporation = yes # Art Center of Florence in Trecento -> Florentine School
warehouse = yes
temple = yes
road_network = yes
fort_14th = yes
small_university = yes # Founded 1321

extra_cost = 15

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech #AdL: was part of the HRE

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_luxury_cloth
		duration = -1
	}
}
#1348 Black Death kills 50-75%popul. in one year
1520.5.5 = {
	base_tax = 23
	base_production = 8
	base_manpower = 2
	fine_arts_academy = yes
	art_corporation = no
	#library = yes # Laurentian Library
}
1527.1.1 = {
	controller = SPA
} # War of the League of Cognac
1529.8.3 = {
	controller = FIR
} # Treaty of Cambrai
1530.1.1 = {
	road_network = no
	paved_road_network = yes
	fort_14th = yes
}
1530.2.27 = {
	hre = no
}
1569.1.1 = {
	owner = TUS
	controller = TUS
	add_core = TUS
	remove_core = FIR
} # Pope Pius V declared Duke Cosimo I de' Medici  Grand Duke of Tuscany
1618.1.1 = {
	hre = no
}
1660.1.1 = {
	fine_arts_academy = no
	opera = yes
}
1801.2.9 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # The Treaty of LunÚville
1801.3.21 = {
	owner = ETR
	controller = ETR
	add_core = ETR
} # The Kingdom of Etruria
1807.12.10 = {
	owner = FRA
	controller = FRA
	remove_core = ETR
} # Etruria is annexed to France
1814.4.11 = {
	owner = TUS
	controller = TUS
	remove_core = FRA
} # Napoleon abdicates and Tuscany is restored
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
