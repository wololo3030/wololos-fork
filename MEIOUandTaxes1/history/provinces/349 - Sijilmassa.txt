# 349 - Sijilmassa

owner = TFL
controller = TFL
add_core = TFL

capital = "Sijilmassa"
trade_goods = palm_date
culture = tamazight
religion = sunni

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = muslim
discovered_by = western
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1204.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = copper
}
1356.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1530.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = {
	unrest = 5
} # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = {
	unrest = 0
}
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
