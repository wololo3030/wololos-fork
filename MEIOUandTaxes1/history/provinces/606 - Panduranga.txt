# 606 - Panduranga
# LS - Indochina set up
# TM-Smiles indochina-mod for meiou

owner = CHA
controller = CHA
add_core = CHA

capital = "Panduranga"
trade_goods = rice
culture = cham
religion = hinduism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese
discovered_by = indian
discovered_by = muslim

1501.1.1 = {
	base_tax = 4
}
1535.1.1 = {
	discovered_by = POR
}
1883.8.25 = {
	owner = FRA
	controller = FRA
}
