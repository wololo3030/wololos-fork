# 3382 - Vicensa

owner = VER
controller = VER
add_core = VER

capital = "Vicenza"
trade_goods = wine
culture = venetian
religion = catholic

hre = yes

base_tax = 9
base_production = 2
base_manpower = 0

is_city = yes
urban_infrastructure_1 = yes
workshop = yes
local_fortification_1 = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
#1356.1.1 = {
#	#add_core = VEN
#	#add_core = MLO
#}
1404.1.1 = {
	controller = VEN
	owner = VEN
	add_core = VEN
	remove_core = VER
}
1508.12.1 = {
	add_core = HAB
} # War of the League of Cambrai
1509.6.1 = {
	controller = HAB
} # Venice collapses
1511.10.4 = {
	controller = VEN
}
1513.6.23 = {
	controller = HAB
}
1516.12.1 = {
	controller = VEN
	remove_core = HAB
} # Treaty of Brussels
1520.5.5 = {
	base_tax = 10
	base_production = 3
	base_manpower = 1
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
	#remove_core = MLO
	hre = no
}
1618.1.1 = {
	hre = no
}
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
	remove_core = HAB
} # Transpadane Republic
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
# 1805.3.17 - Kingdom of Italy
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = HAB
}
