# 1261 - Lieksa (Pohjois-Karjala)

owner = NOV
controller = NOV
add_core = NOV

capital = "Lieksa"
trade_goods = fur
culture = karelian
religion = orthodox

hre = no

base_tax = 0
base_production = 0
base_manpower = 0

is_city = yes

native_size = 5
native_ferocity = 2
native_hostileness = 1

discovered_by = western
discovered_by = eastern
 
450.1.1 = {
	add_permanent_province_modifier = {
		name = "6000_population"
		duration = -1
	}
	set_province_flag = freeholders_control_province
}
1400.1.1 = {
	remove_province_modifier = "6000_population"
	add_permanent_province_modifier = {
		name = "4000_population"
		duration = -1
	}
}
1478.1.14 = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
}
1547.1.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1550.1.1 = {
	remove_province_modifier = "4000_population"
	base_tax = 1
}
1611.1.1 = {
	controller = SWE
}
1617.2.17 = {
	owner = SWE
	add_core = SWE
	capital = "Kiruna"
}
1740.1.1 = {
	fort_14th = yes
}
1800.1.1 = {
	fort_14th = yes
}
1710.9.9 = {
	controller = RUS
} # The Great Nordic War-Captured Keksholm
1721.8.30 = {
	controller = SWE
	remove_core = RUS
} # The Peace of Nystad
1809.3.29 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SWE
} # Treaty of Fredrikshamn
1809.9.17 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = SWE
}
1850.1.1 = {
	base_tax = 9
}
