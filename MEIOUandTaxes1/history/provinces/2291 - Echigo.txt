# 2291 - Echigo
# GG/LS - Japanese Civil War

owner = USG
controller = USG
add_core = USG

capital = "Kasugayama"
trade_goods = hemp
culture = koshi
religion = mahayana #shinbutsu

hre = no

base_tax = 41
base_production = 2
base_manpower = 3

is_city = yes
marketplace = yes
temple = yes
harbour_infrastructure_2 = yes
local_fortification_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 71
	base_production = 5
	base_manpower = 4
}
1542.1.1 = {
	discovered_by = POR
}
1600.9.15 = {
	owner = DTE
	controller = DTE
} # Battle of Sekigahara
1630.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
