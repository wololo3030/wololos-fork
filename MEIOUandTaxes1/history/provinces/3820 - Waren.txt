# 3820 - Waren

owner = WRL
controller = WRL
add_core = WRL

capital = "Waren"
trade_goods = wheat #linen
culture = pommeranian
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1436.9.8 = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1500.1.1 = {
	road_network = yes
}
1530.1.1 = {
	religion = protestant
}
1695.1.1 = {
	owner = MKL
	controller = MKL
	add_core = MKL
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
