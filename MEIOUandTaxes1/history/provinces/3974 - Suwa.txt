# 3974 - Suwa (Southern Shinano)

owner = SUW
controller = SUW
add_core = SUW

capital = "Iida"
trade_goods = rice
culture = chubu
religion = mahayana

hre = no

base_tax = 11
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese

1356.1.1 = {
	add_core = OGA
}
1366.1.1 = {
	add_core = USG
	controller = USG
	owner = USG
}
1384.1.1 = {
	add_core = SBA
	controller = SBA
	owner = SBA
}
1399.1.1 = {
	controller = OGA
	owner = OGA
}
1401.1.1 = {
	controller = SUW
	owner = SUW
} # Murakami uprising
1425.1.1 = {
	controller = OGA
	owner = OGA
}
1471.1.1 = { } #Uyesugi had a split rule of shinano until 1477(?)
1485.1.1 = {
	controller = SUW
	owner = SUW
} # Suwa uprising starts after Onin war
1501.1.1 = {
	base_tax = 20
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1542.1.3 = {
	owner = TKD
	controller = TKD
} # Suwa taken by Takeda
1582.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
