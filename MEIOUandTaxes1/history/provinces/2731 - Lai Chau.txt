# 2731 - Lai Chau

owner = DLI
controller = DLI
add_core = DLI

capital = "An Tay"
trade_goods = lumber
culture = sipsong_thai
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese
discovered_by = indian

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_core = DAI
}
1383.1.1 = {
	owner = LXA
	controller = LXA
	add_core = LXA
	remove_core = DLI
}
1427.1.1 = {
	owner = DAI
	controller = DAI
	remove_core = LXA
}
1540.1.1 = {
	owner = ANN
	controller = ANN
	add_core = ANN
}
1545.1.1 = {
	owner = TOK
	controller = TOK
	add_core = TOK
	remove_core = ANN
} # Nguyen Kim dies
1730.1.1 = {
	unrest = 5
} # Peasant revolt
1731.1.1 = {
	unrest = 0
}
1769.1.1 = {
	unrest = 6
} # Tay Son Rebellion
1776.1.1 = {
	unrest = 0
	owner = DAI
	controller = DAI
	add_core = DAI
} # Tay Son Dynasty conquered the Nguyen Lords
1786.1.1 = {
	unrest = 5
} # Unsuccessful revolt
1787.1.1 = {
	unrest = 0
}
1802.7.22 = {
	owner = ANN
	controller = ANN
	remove_core = DAI
} # Nguyen Phuc Anh conquered the Tay Son Dynasty
1883.8.25 = {
	owner = FRA
	controller = FRA
}
