# 2615 - Zollern

owner = HOH
controller = HOH
add_core = HOH

capital = "Hechingen"
trade_goods = wheat
culture = schwabisch
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	set_province_flag = freeholders_control_province
}
1500.1.1 = {
	road_network = yes
}
1733.1.1 = {
	unrest = 2
	base_tax = 5
	base_production = 5
} # Karl Alexander becomes Duke of Würtemberg. He is a catholic with a jewish  advisor, which is very much resented by the protestant nobility.
1737.3.12 = {
	unrest = 0
} # Death of the Duke, execution of the advisor after a set up process.
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1849.9.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HOH
}
