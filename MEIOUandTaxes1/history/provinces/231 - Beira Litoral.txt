# 231 - Beira Litoral

owner = POR
controller = POR
add_core = POR

capital = "Coimbra"
trade_goods = fish
culture = portugese
religion = catholic

hre = no

base_tax = 16
base_production = 1
base_manpower = 1

is_city = yes
road_network = yes
local_fortification_1 = yes
temple = yes
harbour_infrastructure_1 = yes
small_university = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1250.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1372.5.5 = {
	unrest = 4
} # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5 = {
	unrest = 0
} # Civil unrest repressed.
1384.6.30 = {
	controller = CAS
} # Portuguese Interregnum
1384.11.30 = {
	controller = POR
} # CAS retreats definetly.
1420.1.1 = {
	base_tax = 12
}
1438.1.1 = {
	add_permanent_province_modifier = {
		name = the_batalha_monastery
		duration = -1
	}
}
1500.3.3 = {
	base_tax = 18
	base_production = 2
	base_manpower = 1
}
1515.1.1 = {
	road_network = no
	paved_road_network = yes
}
1580.8.25 = {
	controller = SPA
}
1580.8.26 = {
	controller = POR
}
1640.1.1 = {
	unrest = 8
} # Revolt headed by John of Bragan�a
1640.12.1 = {
	unrest = 0
}
1807.11.30 = {
	controller = FRA
} # Occupied by France
1808.8.30 = {
	controller = POR
}
1810.7.25 = {
	controller = FRA
} # Invaded after the battle of C�a
1811.1.1 = {
	controller = POR
} # The Napoleonic army retreats from Portugal
