# 1366 - W�rzburg

owner = WBG
controller = WBG
add_core = WBG

capital = "W�rzburg"
trade_goods = wine
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 14
base_production = 1
base_manpower = 1

is_city = yes
temple = yes
town_hall = yes
local_fortification_1 = yes
workshop = yes
warehouse = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 15
	base_production = 2
	base_manpower = 1
}
1647.1.1 = {
	early_modern_university = yes
} # (existed until 1803)
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1814.4.11 = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = WBG
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
