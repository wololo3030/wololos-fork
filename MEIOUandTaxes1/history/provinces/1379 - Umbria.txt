# 1379 - Umbria

owner = PAP
controller = PAP
add_core = PAP

capital = "Spoleto"
trade_goods = wine
culture = umbrian
religion = catholic

hre = no

base_tax = 5
base_production = 6
base_manpower = 1

is_city = yes
temple = yes
urban_infrastructure_2 = yes
marketplace = yes
workshop = yes
art_corporation = yes # Umbrian School
local_fortification_1 = yes
road_network = yes
fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1309.1.1 = {
	add_core = PA2
	owner = PA2
	controller = PA2
}
1337.1.1 = {
	revolt = {
		type = noble_rebels
		size = 0
	}
	controller = REB
}
1354.1.1 = {
	revolt = { }
	controller = PA2
}
1378.1.1 = {
	remove_core = PA2
	owner = PAP
	controller = PAP
}
1441.1.1 = {
	add_permanent_province_modifier = {
		name = "republic_of_cospaia"
		duration = -1
	}
	road_network = no
	paved_road_network = yes
}
1503.9.1 = {
	revolt = {
		type = anti_tax_rebels
		size = 0
	}
	controller = REB
} # Loss of Papal authority after the death of Alexander III, Venetian influence
1506.1.1 = {
	revolt = { }
	controller = PAP
}
1520.5.5 = {
	base_tax = 9
	base_production = 4
	base_manpower = 1
	fort_14th = yes
}
1805.3.17 = {
	owner = ITA
	controller = ITA
	add_core = ITA
} # Treaty of Pressburg
1814.4.11 = {
	owner = PAP
	controller = PAP
	remove_core = ITA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
