# XNG - Wei (was Xiang)
# LS - Chinese Civil War

government = chinese_monarchy_5 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = jianghuai
capital = 696

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -2 }
	add_absolutism = -100
	add_absolutism = 70
}

1356.1.1   = {
	monarch = {
		name = "Zaozhu"
		dynasty = "Peng"
		ADM = 3
		DIP = 3
		MIL = 4
		birth_date = 1328.1.1
	}
	set_country_flag = red_turban_reb
	leader = {
		name = "Junyong Zhao"
		type = general
		fire = 2
		shock = 3
		manuever = 2
		siege = 1
		death_date = 1389.1.1
	}
}