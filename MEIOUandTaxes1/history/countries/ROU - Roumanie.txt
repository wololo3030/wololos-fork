# ROU - Roumanie

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = vlach
add_accepted_culture = moldovian
add_accepted_culture = transylvanian
religion = orthodox
technology_group = eastern
capital = 161

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}
