# Wu
# WUU

government = chinese_monarchy_2 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = jianghuai
capital = 2613	# Anqing

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -2 }
	add_absolutism = -100
	add_absolutism = 70
}

1348.8.1   = {
	monarch = {
		name = "Pusheng"
		dynasty = "Zhao"
		ADM = 3
		DIP = 1
		MIL = 6
		birth_date = 1314.10.21
	}
	set_country_flag = no_forbidden_city
	set_country_flag = red_turban_reb
}