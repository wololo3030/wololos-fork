# LNG - Liang
# LS - Chinese Civil War
# 2010-jan-21 - FB - HT3 changes

government = chinese_monarchy_2 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = jingchu
capital = 662 #Jianshui

historical_rival = SNG
historical_rival = QII

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -5 }
	add_absolutism = -100
	add_absolutism = 100
	add_country_modifier = { name = mongol_xingsheng duration = -1 }
}


1346.1.1 = {
	monarch = {
		name = "Vajravarmi"
		dynasty = "Kublaid"
		culture = mongol
		ADM = 5
		DIP = 3
		MIL = 6
	}
}
