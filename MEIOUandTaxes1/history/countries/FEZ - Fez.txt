# FEZ - Fez

government = feudal_monarchy government_rank = 1
mercantilism = 10

primary_culture = rifain
add_accepted_culture = fassi
religion = sunni
technology_group = muslim
capital = 343	# Fez

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	add_country_modifier = { name = fez_political_anarchy duration = 3600 }
	set_country_flag = title_5
	set_country_flag = ruler_versus_church
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 0
	#add_country_modifier = { name = obstacle_shifting_loyalties duration = -1 }
	#add_country_modifier = { name = obstacle_succession duration = -1 }
	#add_country_modifier = { name = obstacle_feudal_fragmentation duration = -1 }
	add_legitimacy = -70
	add_stability = -3
}

1348.1.1 = {
	monarch = {
		name = "Abu 'Inan Faris"
		dynasty = "Marinid"
		ADM = 3
		DIP = 5
		MIL = 5
	}
}

1359.1.1 = {
	monarch = {
		name = "Abu Salim 'Ali II"
		dynasty = "Marinid"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1361.1.1 = {
	monarch = {
		name = "Abdul-Halim"
		dynasty = "Marinid"
		ADM = 1
		DIP = 1
		MIL = 3
	}
}

1362.1.1 = {
	monarch = {
		name = "Abu Zayyan Muhammad III"
		dynasty = "Marinid"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1366.1.1 = {
	monarch = {
		name = "Abu'l-Faris 'Abdul-'Aziz"
		dynasty = "Marinid"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1372.1.1 = {
	monarch = {
		name = "Abu'l-'Abbas Ahmad"
		dynasty = "Marinid"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1374.1.1 = {
	monarch = {
		name = "Musa"
		dynasty = "Marinid"
		ADM = 3
		DIP = 1
		MIL = 3
	}
}

1384.1.1 = {
	monarch = {
		name = "Abu-Zayan Muhammad V"
		dynasty = "Marinid"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1386.1.1 = {
	monarch = {
		name = "Muhammad VI"
		dynasty = "Marinid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1387.1.1 = {
	monarch = {
		name = "Abu'l-'Abbas Ahmad"
		dynasty = "Marinid"
		ADM = 3
		DIP = 3
		MIL = 4
	} #restored
}

1393.1.1 = {
	monarch = {
		name = "Abd'ul-Aziz II"
		dynasty = "Marinid"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1398.1.1 = {
	monarch = {
		name = "'Abdullah"
		dynasty = "Marinid"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1399.1.1 = {
	monarch = {
		name = "'Uthman III"
		dynasty = "Marinid"
		ADM = 4
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "'Abdul-Haqq"
		monarch_name = "'Abdul-Haqq II"
		dynasty = "Marinid"
		birth_Date = 1399.1.1
		death_date = 1465.1.1
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 1
	}
}

1420.1.1 = {
	monarch = {
		name = "'Abdul-Haqq II"
		dynasty = "Marinid"
		ADM = 1
		DIP = 3
		MIL = 1
	}
}

1465.5.24 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Wattassid"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1504.1.1 = {
	monarch = {
		name = "Muhammad II"
		dynasty = "Wattassid"
		ADM = 1
		DIP = 6
		MIL = 2
	}
}

1524.1.1 = {
	monarch = {
		name = "Ahmad"
		dynasty = "Wattassid"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1526.1.1 = {
	monarch = {
		name = "Abu al-Hasan Abu Hasan Ali ibn Muhammad"
		dynasty = "Wattassid"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1526.6.1 = {
	monarch = {
		name = "Abu al-Abbas Ahmad ibn Muhammad"
		dynasty = "Wattassid"
		ADM = 2
		DIP = 3
		MIL = 2
	}
	historical_neutral = TUR
	add_country_modifier = { name = obstacle_military_administration duration = -1 }
	#add_country_modifier = { name = obstacle_magnates duration = -1 }
	add_country_modifier = { name = obstacle_feudal_fragmentation duration = -1 }
}

1545.1.1 = {
	monarch = {
		name = "Nasir ad-Din al-Qasri Muhammad ibn Ahmad"
		dynasty = "Wattassid"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1547.1.1 = {
	monarch = {
		name = "Abu al-Abbas Ahmad ibn Muhammad"
		dynasty = "Wattassid"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1554.1.1 = {
	monarch = {
		name = "Abu al-Hasan Abu Hasun Ali ibn Muh"
		dynasty = "Saadi"
		ADM = 2
		DIP = 3
		MIL = 2
	}
	primary_culture = fassi
	add_accepted_culture = berber
}

1604.1.1 = {
	monarch = {
		name = "Mohammed esh Sheikh el Mamun"
		dynasty = "Saadi"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1613.1.1 = {
	monarch = {
		name = "Abdallah II"
		dynasty = "Saadi"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1613.8.22 = {
	heir = {
		name = "Abd el Malek"
		monarch_name = "Abd el Malek"
		dynasty = "Alaouite"
		birth_Date = 1600.1.1
		death_date = 1626.1.1
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1623.1.1 = {
	monarch = {
		name = "Abd el Malek"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}
