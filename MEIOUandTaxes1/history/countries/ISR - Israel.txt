government = feudal_monarchy government_rank = 1
primary_culture = mizrahi
religion = jewish
technology_group = muslim
capital = 379 #Jerusalem

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = -3 }
	add_absolutism = -100
	add_absolutism = 80
}