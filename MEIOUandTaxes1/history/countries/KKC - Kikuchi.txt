# KKC - Kikuchi
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
# aristocracy_plutocracy = -4
# centralization_decentralization = 2
# innovative_narrowminded = 2
mercantilism = 0.0 # mercantilism_freetrade = -5
# offensive_defensive = -1
# land_naval = 0
# quality_quantity = 4
# serfdom_freesubjects = -5
# isolationist_expansionist = -3
# secularism_theocracy = 0
primary_culture = kyushu
religion = mahayana
technology_group = chinese
capital = 3971

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1319.1.1 = {
	monarch = {
		name = "Takemitsu"
		dynasty = "Kikuchi"
		ADM = 4
		DIP = 5
		MIL = 4
	}
	define_ruler_to_general = {
		fire = 2
		shock = 4
		manuever = 2
		siege = 3
	}
}

1342.1.1 = {
	heir = {
		name = "Takemasa"
		monarch_name = "Takemasa"
		dynasty = "Kikuchi"
		birth_date = 1342.1.1
		death_date = 1374.7.6
		claim = 90
		ADM = 5
		DIP = 2
		MIL = 5
	}
}