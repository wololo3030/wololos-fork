# MOR - Morocco

government = despotic_monarchy government_rank = 1
mercantilism = 10
primary_culture = fassi
add_accepted_culture = tamazight
add_accepted_culture = rifain
add_accepted_culture = chleuh
religion = sunni
technology_group = turkishtech
unit_type = muslim
capital = 4075	# Marrakech

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1348.1.1 = {
	monarch = {
		name = "Abu 'Inan Faris"
		dynasty = "Marinid"
		ADM = 3
		DIP = 5
		MIL = 5
	}
}

1359.1.1 = {
	monarch = {
		name = "Abu Salim 'Ali II"
		dynasty = "Marinid"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1361.1.1 = {
	monarch = {
		name = "Abdul-Halim"
		dynasty = "Marinid"
		ADM = 1
		DIP = 1
		MIL = 3
	}
}

1362.1.1 = {
	monarch = {
		name = "Abu Zayyan Muhammad III"
		dynasty = "Marinid"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1366.1.1 = {
	monarch = {
		name = "Abu'l-Faris 'Abdul-'Aziz"
		dynasty = "Marinid"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1372.1.1 = {
	monarch = {
		name = "Abu'l-'Abbas Ahmad"
		dynasty = "Marinid"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1374.1.1 = {
	monarch = {
		name = "Musa"
		dynasty = "Marinid"
		ADM = 3
		DIP = 1
		MIL = 3
	}
}

1384.1.1 = {
	monarch = {
		name = "Abu-Zayan Muhammad V"
		dynasty = "Marinid"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1386.1.1 = {
	monarch = {
		name = "Muhammad VI"
		dynasty = "Marinid"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1387.1.1 = {
	monarch = {
		name = "Abu'l-'Abbas Ahmad"
		dynasty = "Marinid"
		ADM = 3
		DIP = 3
		MIL = 4
	} #restored
}

1393.1.1 = {
	monarch = {
		name = "Abd'ul-Aziz II"
		dynasty = "Marinid"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1398.1.1 = {
	monarch = {
		name = "'Abdullah"
		dynasty = "Marinid"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1399.1.1 = {
	monarch = {
		name = "'Uthman III"
		dynasty = "Marinid"
		ADM = 4
		DIP = 4
		MIL = 2
	}
	heir = {
		name = "'Abdul-Haqq"
		monarch_name = "'Abdul-Haqq II"
		dynasty = "Marinid"
		birth_Date = 1399.1.1
		death_date = 1465.1.1
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 1
	}
}

1420.1.1 = {
	monarch = {
		name = "'Abdul-Haqq II"
		dynasty = "Marinid"
		ADM = 1
		DIP = 3
		MIL = 1
	}
}

1428.1.1 = {
	monarch = {
		name = "'Abu Zakariyya'Yahya"
		dynasty = "Wattasid"
		ADM = 5
		DIP = 2
		MIL = 3
	}
}

1448.1.1 = {
	monarch = {
		name = "'Al�"
		dynasty = "Wattasid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1458.1.1 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Wattasid"
		ADM = 4
		DIP = 2
		MIL = 4
	}
}

1504.1.1 = {
	monarch = {
		name = "Muhammad II al-Burtuq�l�"
		dynasty = "Wattasid"
		ADM = 1
		DIP = 6
		MIL = 2
	}
}

1510.1.1 = {
	#set_variable = { which = "centralization_decentralization" value = 1 }
	add_absolutism = -100
	add_absolutism = 40
	monarch = {
		name = "Muhammad al-Qa'im"
		dynasty = "Saadi"
		ADM = 4
		DIP = 4
		MIL = 6
	}
	define_ruler_to_general = {
		fire = 4
		shock = 1
		manuever = 3
		siege = 0
	}
	heir = {
		name = "Ahmad al A'raj"
		monarch_name = "Ahmad al A'raj"
		dynasty = "Saadi"
		birth_date = 1490.1.1
		death_date = 1544.1.1
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 5
	}
	define_heir_to_general = {
		fire = 2
		shock = 3
		manuever = 4
		siege = 0
	}
}

1517.1.1 = {
	monarch = {
		name = "Ahmad al A'raj"
		dynasty = "Saadi"
		ADM = 1
		DIP = 3
		MIL = 5
	}
	define_ruler_to_general = {
		fire = 2
		shock = 3
		manuever = 4
		siege = 0
	}
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad II"
		dynasty = "Saadi"
		birth_Date = 1510.1.1
		death_date = 1557.10.24
		claim = 95
		ADM = 4
		DIP = 5
		MIL = 3
	}
	define_heir_to_general = {
		fire = 3
		shock = 3
		manuever = 2
		siege = 0
	}
}

1525.9.27 = {
	monarch = {
		name = "Muhammad II"
		dynasty = "Saadi"
		ADM = 4
		DIP = 5
		MIL = 3
	}
	define_ruler_to_general = {
		fire = 3
		shock = 3
		manuever = 2
		siege = 0
	}
	heir = {
		name = "'Abd All�h"
		monarch_name = "'Abd All�h"
		dynasty = "Saadi"
		birth_date = 1517.1.1
		death_date = 1574.1.24
		claim = 95
		ADM = 6
		DIP = 4
		MIL = 5
	}
}

1530.1.2 = {
	government = administrative_monarchy
}

1557.10.24 = {
	monarch = {
		name = "'Abd All�h"
		dynasty = "Saadi"
		ADM = 6
		DIP = 4
		MIL = 5
	}
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad III"
		dynasty = "Saadi"
		birth_Date = 1557.1.1
		death_date = 1578.8.5
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 2
	}
	define_heir_to_general = {
		fire = 3
		shock = 3
		manuever = 3
		siege = 0
	}
}

1574.1.24 = {
	monarch = {
		name = "Muhammad III"
		dynasty = "Saadi"
		ADM = 1
		DIP = 4
		MIL = 2
	}
	define_ruler_to_general = {
		fire = 3
		shock = 3
		manuever = 3
		siege = 0
	}
	heir = {
		name = "Ahmad"
		monarch_name = "Ahmad II"
		dynasty = "Saadi"
		birth_Date = 1549.1.1
		death_date = 1603.8.20
		claim = 95
		ADM = 3
		DIP = 1
		MIL = 6
	}
}

1578.8.5 = {
	monarch = {
		name = "Ahmad II"
		dynasty = "Saadi"
		ADM = 3
		DIP = 1
		MIL = 6
	}
}

1603.8.20 = {
	monarch = {
		name = "Mohammed esh Sheikh el Mamun"
		dynasty = "Saadi"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "'Abd All�h"
		monarch_name = "'Abd All�h II"
		dynasty = "Saadi"
		birth_Date = 1564.1.1
		death_date = 1609.8.3
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1613.1.1 = {
	monarch = {
		name = "'Abd All�h II"
		dynasty = "Saadi"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "'Abd al-Malik"
		monarch_name = "'Abd al-Malik"
		dynasty = "Saadi"
		birth_Date = 1590.1.1
		death_date = 1631.2.11
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1623.1.1 = {
	monarch = {
		name = "'Abd al-Malik"
		dynasty = "Saadi"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1631.2.11 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad II"
		dynasty = "Alaouite"
		birth_date = 1600.1.1
		death_date = 1666.1.1
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1635.1.1 = {
	monarch = {
		name = "Muhammad II"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Al-Rashid"
		monarch_name = "Al-Rashid"
		dynasty = "Alaouite"
		birth_Date = 1631.1.1
		death_date = 1672.3.28
		claim = 95
		ADM = 5
		DIP = 4
		MIL = 6
	}
	define_heir_to_general = {
		fire = 4
		shock = 3
		manuever = 4
		siege = 1
	}
}

1666.1.1 = {
	monarch = {
		name = "Al-Rashid"
		dynasty = "Alaouite"
		ADM = 5
		DIP = 4
		MIL = 6
	}
	define_ruler_to_general = {
		fire = 4
		shock = 3
		manuever = 4
		siege = 1
	}
}

1672.3.28 = {
	monarch = {
		name = "Al-Harrani"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Ismail"
		monarch_name = "Mawlay Ismail Ibn Sharif"
		dynasty = "Alaouite"
		birth_Date = 1645.1.1
		death_date = 1727.3.22
		claim = 95
		ADM = 6
		DIP = 6
		MIL = 6
	}
	define_heir_to_general = {
		fire = 3
		shock = 2
		manuever = 3
		siege = 1
	}
}

1684.1.1 = {
	monarch = {
		name = "Mawlay Ismail Ibn Sharif"
		dynasty = "Alaouite"
		ADM = 6
		DIP = 6
		MIL = 6
	}
	define_ruler_to_general = {
		fire = 3
		shock = 2
		manuever = 3
		siege = 1
	}
	heir = {
		name = "Ahmad"
		monarch_name = "Abu'l Abbas Ahmad II"
		dynasty = "Alaouite"
		birth_Date = 1677.1.1
		death_date = 1729.3.5
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1727.3.22 = {
	monarch = {
		name = "Abu'l Abbas Ahmad II"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "'Abd All�h"
		monarch_name = "'Abd All�h"
		dynasty = "Alaouite"
		birth_Date = 1678.1.1
		death_date = 1757.11.10
		claim = 95
		ADM = 6
		DIP = 2
		MIL = 2
	}
}

1729.3.7 = {
	monarch = {
		name = "'Abd All�h"
		dynasty = "Alaouite"
		ADM = 6
		DIP = 2
		MIL = 2
	}
}

1735.1.1 = {
	monarch = {
		name = "Ali"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1736.1.1 = {
	monarch = {
		name = "Abdallah"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1737.1.1 = {
	monarch = {
		name = "Mohammed II"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1738.1.1 = {
	monarch = {
		name = "Al-Mostadi"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1740.1.1 = {
	monarch = {
		name = "Abdallah III"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1745.1.1 = {
	monarch = {
		name = "Zin al-Abidin"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1746.1.1 = {
	monarch = {
		name = "Abdallah IV"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Muhammad"
		monarch_name = "Mohammed ben Abdallah"
		dynasty = "Alaouite"
		birth_Date = 1739.3.7
		death_date = 1790.4.12
		claim = 95
		ADM = 5
		DIP = 5
		MIL = 3
	}
}

1757.11.10 = {
	monarch = {
		name = "Mohammed ben Abdallah"
		dynasty = "Alaouite"
		ADM = 5
		DIP = 5
		MIL = 3
	}
	heir = {
		name = "al-Yazid"
		monarch_name = "al-Yazid"
		dynasty = "Alaouite"
		birth_Date = 1750.1.1
		death_date = 1792.2.15
		claim = 95
		ADM = 1
		DIP = 4
		MIL = 1
	}
}

1790.4.12 = {
	monarch = {
		name = "al-Yazid"
		dynasty = "Alaouite"
		ADM = 1
		DIP = 4
		MIL = 1
	}
	heir = {
		name = "Sulaym�n"
		monarch_name = "Sulaym�n"
		dynasty = "Alaouite"
		birth_date = 1760.1.1
		death_date = 1822.1.1
		claim = 95
		ADM = 1
		DIP = 3
		MIL = 4
	}
}

1792.2.15 = {
	monarch = {
		name = "Sulaym�n"
		dynasty = "Alaouite"
		ADM = 1
		DIP = 3
		MIL = 4
	}
	heir = {
		name = "Abderrahmane"
		monarch_name = "Abderrahmane"
		dynasty = "Alaouite"
		birth_date = 1790.1.1
		death_date = 1850.1.1
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 4
	}
}

1822.1.1 = {
	monarch = {
		name = "Abderrahmane"
		dynasty = "Alaouite"
		ADM = 2
		DIP = 3
		MIL = 4
	}
}
