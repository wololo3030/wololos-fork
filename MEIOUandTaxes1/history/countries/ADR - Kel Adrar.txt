# ADR - Kel Adrar

government = tribal_nomads government_rank = 1
mercantilism = 0.0
primary_culture = tuareg
religion = sunni
technology_group = soudantech
capital = 2923 # Tadmekka

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 0
}

1340.1.1 = {
	monarch = { #Fictional
		name = "Umaru"
		dynasty = "Kel Adrar"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}