# No previous file for Nipissing

culture = odawa
religion = totemism
capital = "Nipissing"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 1 
native_hostileness = 6

1649.1.1  = {
	owner = IRO
	controller = IRO
	add_core = IRO
	culture = iroquois
} #Taken by Iroquois in Beaver Wars.
1650.1.1 = {
	owner = OJI
	controller = OJI
	add_core = OJI
	culture = anishinabe
} #Iroquois focus on Lake Ontario, Ojibwe moves in
